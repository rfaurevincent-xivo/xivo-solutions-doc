************
Contributing
************

General information:

.. toctree::
   :maxdepth: 2

   contributing_doc
   debug_asterisk
   debug_daemon
   generate_custom_prompts
   guidelines
   network
   packaging
   profile_python
   style_guide
   translate
   xivo_package_file_structure

Component specific information:

.. toctree::
   :maxdepth: 2

   cti_server/cti_server
   diagrams
   provisioning/provisioning
   sccp
   webi

Community Documentation

.. toctree::
   :maxdepth: 2

   community/community
