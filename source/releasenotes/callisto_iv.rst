************************************
XiVO Callisto Intermediate Versions
************************************

2019.04
=======

Consult the `2019.04 Roadmap <https://projects.xivo.solutions/versions/133>`_.

.. warning:: This intermediate version has the following known problems:

    * upgrade from Aldebaran won't work,
    * you need to create a callright for outgoing call to work (it may be a 'fake' callright applied to nothing),
    * also, on MDS servers, you need to add the outcall service in the docker compose yml file for outgoing call to work.

Components updated: **asterisk**, **config-mgt**, **xivo-agid**, **xivo-confd**, **xivo-confgend**, **xivo-config**, **xivo-dao**, **xivo-dird**, **xivo-install-script**, **xivo-manage-db**, **xivo-outcall**, **xivo-service**, **xivo-upgrade**, **xivo-web-interface**, **xivocc-installer**, **xucserver**

**Web Assistant**

* `#2405 <https://projects.xivo.solutions/issues/2405>`_ - allow to send flashtext to other connected users 

**WebRTC**

* `#1440 <https://projects.xivo.solutions/issues/1440>`_ - Remove DISABLE_WEBRTC option as it's not needed anymore

**XiVO PBX**

* `#1548 <https://projects.xivo.solutions/issues/1548>`_ - [WEBI] Affichage des numéros libres dans les appels entrants

  .. important:: **Behavior change** Suggestions in webi are now limited to the 10 first available results

* `#2371 <https://projects.xivo.solutions/issues/2371>`_ - Cannot create func key for agent when created with confd API (Bugfix port to callisto)

  .. important:: **Behavior change** When creating a user using the REST API, the CTI profile is now set to a default value and the CTI client is enabled when a CTI client login and a password is set.

* `#2380 <https://projects.xivo.solutions/issues/2380>`_ - Outcall application - check call rights
* `#2382 <https://projects.xivo.solutions/issues/2382>`_ - Add srcnum as available information when receiving a fax
* `#2394 <https://projects.xivo.solutions/issues/2394>`_ - Can not dial numbers with 1 digit or \* + 1 digit (X OR \*X) from uc assistant
* `#2395 <https://projects.xivo.solutions/issues/2395>`_ - Database schema may not be upgraded during upgrade
* `#2396 <https://projects.xivo.solutions/issues/2396>`_ - XDS - Remove all_mds entry from mediaserver and trunkfeatures table

  .. important:: **Behavior change** No more possibility to define a global trunk, a trunk is by definition now attached to one MDS only. If you want a global trunk, this will now be achieved thanks to route configuration.

* `#2403 <https://projects.xivo.solutions/issues/2403>`_ - Outcall application - routing db evolution
* `#2404 <https://projects.xivo.solutions/issues/2404>`_ - XDS - Generate peer configuration from mediaserver list

  .. important:: **Behavior change** It is no longer needed to add SIP peer for each MDS manually. Adding a media server is enough. When upgrading an existing MDS, you should remove manual trunk configuration for each mediaserver.

* `#2408 <https://projects.xivo.solutions/issues/2408>`_ - Dockerize postgres and migrate database to version 11
* `#2415 <https://projects.xivo.solutions/issues/2415>`_ - Confgend can't generate configuration due to sqlalchemy changes for security purposes


2019.03
=======

Consult the `2019.03 Roadmap <https://projects.xivo.solutions/versions/132>`_.

Components updated: **asterisk**, **config-mgt**, **xivo-agid**, **xivo-confgend**, **xivo-config**, **xivo-dao**,
**xivo-libsccp**, **xivo-manage-db**, **xivo-outcall**, **xivo-res-freeze-check**, **xivo-web-interface**,
**xivocc-installer**, **xucmgt**, **xucserver**

**Asterisk**

* `#2222 <https://projects.xivo.solutions/issues/2222>`_ - Prepare Asterisk 16 with XiVO patches
* `#2388 <https://projects.xivo.solutions/issues/2388>`_ - Disable Asterisk start during installation

**WebRTC**

* `#2372 <https://projects.xivo.solutions/issues/2372>`_ - WebRTC - RTP flow is stopped after unhold

**XiVO PBX**

* `#2270 <https://projects.xivo.solutions/issues/2270>`_ - Users groups can be located on any MDS
* `#2345 <https://projects.xivo.solutions/issues/2345>`_ - Add site to existing webi group
* `#2346 <https://projects.xivo.solutions/issues/2346>`_ - Migration of existing groups
* `#2347 <https://projects.xivo.solutions/issues/2347>`_ - Generate groups configuration in confgend
* `#2348 <https://projects.xivo.solutions/issues/2348>`_ - Allow call routing between mds groups
* `#2359 <https://projects.xivo.solutions/issues/2359>`_ - Be able to set mds0 only for SIP Trunk configuration
* `#2361 <https://projects.xivo.solutions/issues/2361>`_ -  Generate SIP registration only for local trunks of the MDS
* `#2369 <https://projects.xivo.solutions/issues/2369>`_ - Outcall application - schedule are checked against wrong time
* `#2370 <https://projects.xivo.solutions/issues/2370>`_ - Outcall application - should survive to asterisk restart
* `#2376 <https://projects.xivo.solutions/issues/2376>`_ - Outcall application - schedule to consider days and months
* `#2377 <https://projects.xivo.solutions/issues/2377>`_ - [C] Incoming call used customized Goto call leads to user not having its ringing time take into account
* `#2378 <https://projects.xivo.solutions/issues/2378>`_ - Set asterisk default language to fr_FR

  .. important:: **Behavior change** Default asterisk language was set to *fr_FR*. In this language we have all sound files.
    To change it to english, one should:

    * verify that the packages `asterisk-sounds-wav-en-us`, `xivo-sounds-en-us` are installed
    * and set, in file :file:`/etc/asterisk/asterisk.conf` the *defaultlanguage* parameter to `en_US`


* `#2385 <https://projects.xivo.solutions/issues/2385>`_ - Filter's users of a group that are not on the same MDS

  .. important:: **Behavior change** Displaying of extension identity in administration interface was changed

    from: `number@context`
    to: `number@mediaserver [context]`


2019.02
=======

Consult the `2019.02 Roadmap <https://projects.xivo.solutions/versions/130>`_.

Components updated: **config-mgt**, **recording-server**, **xivo-agid**, **xivo-confgend**, **xivo-config**, **xivo-dao**, **xivo-install-script**, **xivo-manage-db**, **xivo-provd-plugins**, **xivo-service**, **xivo-web-interface**, **xivocc-installer**, **xucmgt**, **xucserver**

**CCAgent**

* `#2318 <https://projects.xivo.solutions/issues/2318>`_ - Agent name is not indented in CCAgent
* `#2331 <https://projects.xivo.solutions/issues/2331>`_ - Callbacks are removed from the list if I click again on the Callbacks view

**CCManager**

* `#2332 <https://projects.xivo.solutions/issues/2332>`_ - Group view is not refreshed if agent's group changed via ccmanager
* `#2352 <https://projects.xivo.solutions/issues/2352>`_ - [C] CCManager add callbacks count and oldest callback for monitoring purpose

**Desktop Assistant**

* `#2343 <https://projects.xivo.solutions/issues/2343>`_ - Desktop assistant language is hardcoded
* `#2354 <https://projects.xivo.solutions/issues/2354>`_ - German UI translations

**Web Assistant**

* `#2334 <https://projects.xivo.solutions/issues/2334>`_ - Changing ringing device doesn't work

**XUC Server**

* `#2333 <https://projects.xivo.solutions/issues/2333>`_ - Phone status incorrect after xuc restart
* `#2342 <https://projects.xivo.solutions/issues/2342>`_ - xuc does not re-connect to the right IP address of XiVO for the AMi
* `#2349 <https://projects.xivo.solutions/issues/2349>`_ - When leaving an outbound queue, agent can not longer emit a call
* `#2360 <https://projects.xivo.solutions/issues/2360>`_ - [C] - When xuc loses connection to AMI (Ami failure) ghost call may appear in user interfaces

**XiVO PBX**

* `#2324 <https://projects.xivo.solutions/issues/2324>`_ - XDS - Routing should not try to call None
* `#2338 <https://projects.xivo.solutions/issues/2338>`_ - Add column Site to SIP Trunk configuration
* `#2353 <https://projects.xivo.solutions/issues/2353>`_ - Generate sip configuration with local trunks on a MDS
* `#2357 <https://projects.xivo.solutions/issues/2357>`_ - Install xivo-outcall application on XiVO

**XiVO Provisioning**

* `#2326 <https://projects.xivo.solutions/issues/2326>`_ - Yealink phones with v84 plugin can't start attended transfer from CC Agent or UC Assistant

**XiVOCC Infra**

* `#2293 <https://projects.xivo.solutions/issues/2293>`_ - Add NGINX configuration to access all CC services on standard ports 80/443

  .. important:: **Behavior change** Recording server API URL was changed. It is now prefixed with *recording*. For example */records/search* URL was changed to */recording/records/search*.

* `#2294 <https://projects.xivo.solutions/issues/2294>`_ - Generate application secret for Play apps in docker
* `#2330 <https://projects.xivo.solutions/issues/2330>`_ - Access CC services running on multiple servers from one Fingerboard

  .. important:: **Behavior change** 

     * Fingerboard now runs inside the nginx container and the fingerboard container was removed
     * XiVO CC services are opened on URLs without port number
     * CC Assistant, CC Manager, Recording and Config Mgt open through https
     * XiVO CC services running on separate servers can be accessed from one fingerboard
     * See :ref:`nginx_path_distribution` for details


2019.01
=======

Consult the `2019.01 Roadmap <https://projects.xivo.solutions/versions/124>`_.

Components updated: **xivo-agid**, **xivo-confd**, **xivo-confgend**, **xivo-config**, **xivo-db-replication**, **xivo-provd-plugins**, **xivo-provisioning**, **xivo-service**, **xivo-sysconfd**, **xivo-web-interface**, **xivocc-installer**, **xucmgt**, **xucserver**

**Asterisk**

* `#2082 <https://projects.xivo.solutions/issues/2082>`_ - Asterisk - Add a pjproject.conf file
* `#2297 <https://projects.xivo.solutions/issues/2297>`_ -  Asterisk 16 - Add a console.conf file

**CCAgent**

* `#2295 <https://projects.xivo.solutions/issues/2295>`_ - Callbacks cannot be displayed if agent doesn't have right on any queue that contains callback requests

**CCManager**

* `#2286 <https://projects.xivo.solutions/issues/2286>`_ - [C] - CCManager displays wrong logout time in Agent View

**Desktop Assistant**

* `#2313 <https://projects.xivo.solutions/issues/2313>`_ - Tray icon sometimes disapears from notification bar

**Web Assistant**

* `#2301 <https://projects.xivo.solutions/issues/2301>`_ - Second call Bip is not always working and global key not always working

**WebRTC**

* `#2162 <https://projects.xivo.solutions/issues/2162>`_ - Select ringtone output media device

**XUC Server**

* `#2290 <https://projects.xivo.solutions/issues/2290>`_ - Agent state not properly initialized after XUC start
* `#2312 <https://projects.xivo.solutions/issues/2312>`_ - WebAssistant - XuC - xivo-auth User cannot toggle DND or enable/disable forward because his token doesn't exist anymore

**XiVO PBX**

* `#1819 <https://projects.xivo.solutions/issues/1819>`_ - XDS - Import or modify user line site
* `#2124 <https://projects.xivo.solutions/issues/2124>`_ - Be able to run db_replic on both XiVO master and slave in HA
* `#2135 <https://projects.xivo.solutions/issues/2135>`_ - Remove the non answer option "busy" which is useless for queue

  .. important:: **Behavior change** The **Busy** case in the *No answer* tab of Groups and Queues was removed (it was not used at all).

* `#2221 <https://projects.xivo.solutions/issues/2221>`_ - XDS - Provisionning - Being able to synchronize a device on a MDS
* `#2242 <https://projects.xivo.solutions/issues/2242>`_ - XDS - Dial users in different context attached to a different mds fails

**XiVO Provisioning**

* `#2192 <https://projects.xivo.solutions/issues/2192>`_ - Yealink - Update firmware for Yealink phones
* `#2244 <https://projects.xivo.solutions/issues/2244>`_ - Add XiVO logo on T46 and T48 yealink phones
