.. _xivosolutions_release:

*************
Release Notes
*************

.. _callisto_release:

Callisto (2019.05)
==================

Below is a list of *New Features* and *Behavior Changes* compared to the previous LTS version, Boréalis (2018.16).


Callisto.00
-----------

New Features
^^^^^^^^^^^^
* CC Agent :

  * Can display charts on click on statistics buttons - see :ref:`ccagent_statistics`.
* CC Manager:

  * Add callbacks count and oldest callbacks
* Desktop Assistant:

  * Tray icon shows:

    * if user is disconnected
    * if user has missed calls
  * Improved sound settings for WebRTC
  * Windows location and sized is saved when exiting
* UC Assistant:

  * Can empty the search box
* Translation: new German translation of Application (Desktop Assistant, UC Assistant, CC Agent and CC Manager)
* WebRTC

  * Ability to chose which device (e.g. speaker or headset) will be used when ringing - see :ref:`UC - Ringing Device Selection <uc_webrtc_ringing_device>` or :ref:`CC Agent - Ringing Device Selection <agent_webrtc_ringing_device>`.
* XiVO PBX

  * New outgoing calls configuration with *Routes*: more flexible and compatible with XDS sytem - see :ref:`outgoing_calls`.
  * Add SRCNUM as available information for FaxToMail application
  * WebI: available incoming calls number displayed when creating a new Incoming call (suggestions is limited to the 10 first available results).
* XDS

  * Can call a user in different context
  * Can call a group located on any MDS
  * Can synchronize a device from Webi whatever its MDS
  * Can specify the user line site when importing users with a CSV file - see :ref:`user_import`.
  * Can specify local SIP trunks for a MDS
  * Intra-MDS routing SIP peers are auto-generated
  * Outgoing call routes can be configured per-MDS
* High availibility

  * DB Replic can replicate events from the slave XiVO to the XiVO CC reporting database - see :ref:`ha_interconnection_with_cc`
* System

  * Upgrade to asterisk 16, the latest LTS version of asterisk.
  * Upgrade to postgres 11, the latest release of postgres.


Behavior Changes
^^^^^^^^^^^^^^^^

* API

  * Recording server API URL was changed. It is now prefixed with *recording*. For example */records/search* URL
    was changed to */recording/records/search*.
  * When creating a user using the REST API, the CTI profile is now set to a default value and the CTI client is enabled when a CTI client login and a password is set.
* Fingerboard

  * It now runs inside the nginx container and the fingerboard container was removed
  * XiVO CC services are opened on URLs without port number
  * CC Assistant, CC Manager, Recording and Config Mgt open through https
  * XiVO CC services running on separate servers can be accessed from one fingerboard
  * See :ref:`nginx_path_distribution` for details
* System

  * Database will be upgraded from postgres 9.4 to 11
  * Database is now run inside a container
* XiVO PBX

  * Asterisk: language now defaults to fr_FR. 
    To change it to english, one should:

    * verify that the packages `asterisk-sounds-wav-en-us`, `xivo-sounds-en-us` are installed
    * and set, in file :file:`/etc/asterisk/asterisk.conf` the *defaultlanguage* parameter to `en_US`

  * **IAX trunks** are no longer supported.
  * Outgoing calls were migrated to **Routes**: a more flexible routing system - see :ref:`our migration guide <callisto_route_upgrade_guide>`.
  * Web Interface, Groups and Queues configuration: the **Busy** case in the *No answer* tab was removed.
  * WebI : user's in select box are now displayed `number@mediaserver [context]` (instead of `number@context`)
* XDS:

  * Intra-MDS routing SIP peers are auto-generated: you MUST then remove the peers you would have created manually.


Upgrade
^^^^^^^

Follow the usual procedures (**don't forget** the specific steps to upgrade to another LTS version - see :ref:`upgrade_lts_manual_steps`):

* :ref:`XiVO PBX upgrade procedure <upgrade>`
* :ref:`XiVO CC upgrade procedure <upgrade_cc>`
* :ref:`XDS upgrade procedure <upgrade_xds>`

Features & bugfixes list
^^^^^^^^^^^^^^^^^^^^^^^^

Callisto Bugfixes Versions
==========================

Components version table
------------------------

Table listing the current version of the components.

+----------------------+----------------+
| Component            | current ver.   |
+=======================================+
| **XiVO**                              |
+----------------------+----------------+
| XiVO PBX             | 2019.05.02     |
+----------------------+----------------+
| config_mgt           | 2019.05.02     |
+----------------------+----------------+
| db                   | 2019.05.02     |
+----------------------+----------------+
| outcall              | 2019.05.02     |
+----------------------+----------------+
| db_replic            | 2019.05.00     |
+----------------------+----------------+
| **XiVO CC**                           |
+----------------------+----------------+
| elasticsearch        | 1.7.2          |
+----------------------+----------------+
| kibana_volume        | 2019.05.00     |
+----------------------+----------------+
| nginx                | 2019.05.02     |
+----------------------+----------------+
| pack-reporting       | 2019.05.00     |
+----------------------+----------------+
| pgxivocc             | 1.3            |
+----------------------+----------------+
| recording-rsync      | 1.0            |
+----------------------+----------------+
| recording-server     | 2019.05.02     |
+----------------------+----------------+
| spagobi              | 2019.05.00     |
+----------------------+----------------+
| xivo-full-stats      | 2019.05.00     |
+----------------------+----------------+
| xuc                  | 2019.05.02     |
+----------------------+----------------+
| xucmgt               | 2019.05.02     |
+----------------------+----------------+


Callisto.02
-----------

Consult the `Callisto.02 Roadmap <https://projects.xivo.solutions/versions/143>`_.

Components updated: **config-mgt**, **nginx**, **recording-server**, **xivo-confgend**, **xivo-dao**, **xivo-db**, **xivo-manage-db**, **xivo-outcall**, **xivo-web-interface**, **xivocc-installer**, **xucmgt**, **xucserver**

**Desktop Assistant**

* `#2212 <https://projects.xivo.solutions/issues/2212>`_ - Empty the search box

**Recording**

* `#2488 <https://projects.xivo.solutions/issues/2488>`_ - Update login page to have same look and feel than ccagent or cccmanager and display logged username

**Web Assistant**

* `#2506 <https://projects.xivo.solutions/issues/2506>`_ - XDS - status of phone is randomly correct on UC
* `#2507 <https://projects.xivo.solutions/issues/2507>`_ - display flashtext from other users

**XUC Server**

* `#2470 <https://projects.xivo.solutions/issues/2470>`_ - ACD outbound call status is always dialing
* `#2505 <https://projects.xivo.solutions/issues/2505>`_ - Add username to RichDirectoryResult in XUC

**XiVO PBX**

* `#2458 <https://projects.xivo.solutions/issues/2458>`_ - XDS - Improve postgresql configuration handling
* `#2473 <https://projects.xivo.solutions/issues/2473>`_ - XDS - mds installation may fail when configuring uuid
* `#2498 <https://projects.xivo.solutions/issues/2498>`_ - Deleting trunk used in outcall causes outcall not to be
* `#2500 <https://projects.xivo.solutions/issues/2500>`_ - Postgres in docker is always restarting in auto recovery mode
* `#2504 <https://projects.xivo.solutions/issues/2504>`_ - Increase default number of connection in db container
* `#2512 <https://projects.xivo.solutions/issues/2512>`_ - Outcall - no group id for user causes sql group query to fail

**XiVOCC Infra**

* `#2383 <https://projects.xivo.solutions/issues/2383>`_ - XiVO CC services can't use domain names


Callisto.01
-----------

Consult the `Callisto.01 Roadmap <https://projects.xivo.solutions/versions/141>`_.

Components updated: **asterisk**, **xivo-config**, **xivo-db**, **xivo-monitoring**, **xivo-outcall**, **xivo-solutions-doc**, **xivo-upgrade**, **xivo-web-interface**, **xucmgt**


**Desktop Assistant**

* `#2481 <https://projects.xivo.solutions/issues/2481>`_ - Save desktop assistant windows location and size on exit

**WebRTC**

* `#2389 <https://projects.xivo.solutions/issues/2389>`_ - Optimize Chrome WebRTC settings

**XiVO PBX**

* **Asterisk**: Update asterisk to 16.3.0 `#2483 <https://projects.xivo.solutions/issues/2483>`_

  * `#2465 <https://projects.xivo.solutions/issues/2465>`_ - Asterisk 16 - Voicemail supervision doesn't work
* `#2362 <https://projects.xivo.solutions/issues/2362>`_ - XDS - Database schema is shown as NOK after upgrade
* `#2453 <https://projects.xivo.solutions/issues/2453>`_ - Outcall - application - consider context inclusion
* `#2460 <https://projects.xivo.solutions/issues/2460>`_ - Outcall - make intra-mds call routing work for other contexts than default
* `#2463 <https://projects.xivo.solutions/issues/2463>`_ - Outcall - migration to Route - migration script creates routes with outgoing context
* `#2464 <https://projects.xivo.solutions/issues/2464>`_ - Outcall - application does not reconnect to database
* `#2477 <https://projects.xivo.solutions/issues/2477>`_ - Permissions not respected when calling a forwared user
* `#2478 <https://projects.xivo.solutions/issues/2478>`_ - Asterisk 16 - Not logging CEL if database is not ready when asterisk starts
* `#2479 <https://projects.xivo.solutions/issues/2479>`_ - Outcall - Callerid and forward - Wrong callerid when U1 calls U2 fwded to external user
* `#2484 <https://projects.xivo.solutions/issues/2484>`_ - Route - I should be able to create a route with prio > 10
* `#2489 <https://projects.xivo.solutions/issues/2489>`_ - Clean outcall from contextmember table
* `#2491 <https://projects.xivo.solutions/issues/2491>`_ - Bypass schedule with password doesn't work for outgoing calls

Callisto Intermediate Versions
==============================

.. toctree::
   :maxdepth: 2

   callisto_iv
