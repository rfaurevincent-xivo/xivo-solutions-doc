.. _devices:

*******
Devices
*******

In XiVO, there are two kind of devices:

.. toctree::
   :maxdepth: 1

   official_devices
   community_devices

The officially supported devices will be supported across upgrades and phone features are guaranteed
to be supported on the latest version.

The community supported devices are only supported by the community. In other words, maintenance,
bug, corrections and features are developed by members of the XiVO community. XiVO does not
officially endorse support for these devices.

The next topics lists the officially and community supported devices. For each vendor, a table shows
the various features supported by XiVO. Here's an example:

+--------------------------------------------+---------+---------+--------------+
|                                            | Model X | Model Y | Model Z      |
+============================================+=========+=========+==============+
| Provisioning                               | Y       | Y       | Y            |
+--------------------------------------------+---------+---------+--------------+
| H-A                                        | Y       | Y       | Y            |
+--------------------------------------------+---------+---------+--------------+
| Directory XIVO                             | N       | Y       | Y            |
+--------------------------------------------+---------+---------+--------------+
| Funckeys                                   | 0       | 2       | 8            |
+--------------------------------------------+---------+---------+--------------+
|                                            | **Supported programmable keys**  |
+--------------------------------------------+---------+---------+--------------+
| User with supervision function             | Y       | Y       | Y            |
+--------------------------------------------+---------+---------+--------------+

The rows have the following meaning:

Provisioning
   Is the device supported by the :ref:`auto-provisioning <provisioning>` system ?

H-A
   Is the device supported by the :ref:`high availability <high-availability>` system ?

Directory XiVO
   Is the device supported by the :ref:`remote directory <remote-directory>` ? In other word, is it
   possible to consult the XiVO's remote directory from the device ?

Funckeys
   How many function keys can be configured on the device from the XiVO web interface ?

   The number of function keys that can be configured on a device is not necessarily the same as
   the number of physical function keys the device has. For example, an Aastra 6757i has 12 physical
   keys but you can configure 30 function keys because of the page system.

Inside a table, the following legend is used:

* Y = Yes / Supported
* N = No / Not supported
* NT = Not tested
* NYT = Not yet tested

Each table also contains a section about the supported function keys. In that section, the following
legend can also be used:

* FK = Funckey
* SK = SoftKey
* HK = HardKey
* MN = Menu

Function keys work using the extensions in :menuselection:`Services --> Extensions`. It is important
to enable the function keys you want to use.  Also, the enable transfer option in the user
configuration services tab must be enabled to use transfer function keys.

.. _devices_releasenotes:

Plugins release notes
=====================

xivo-aastra-3.3.1-SP4-HF9
-------------------------

.. note:: Replaces xivo-aastra-3.3.1-SP4 plugin

:v1.8: Download links for firmwares update for Aastra/Mitel phones.
:v1.7: Firmware update for Aastra/Mitel phones.
    Particularly these new firmwares support forwarding of Multiple Spanning Tree Protocol (MSTP) messages from the LAN port to the PC port.
    This ensures issues regarding network loops are not created.


xivo-aastra-4.3.0
-----------------

.. note:: Replaces xivo-aastra-4.1.0 plugin

:v1.8: Download links for firmwares update for Aastra/Mitel phones.
:v1.7: Firmware update for Aastra/Mitel phones.
    Particularly these new firmwares support forwarding of Multiple Spanning Tree Protocol (MSTP) messages from the LAN port to the PC port.
    This ensures issues regarding network loops are not created.

xivo-cisco-spa8000-6.1.11
-------------------------

:v1.1: Fix download of firmware with procedure :ref:`Cisco SPA8000 download firmware <cisco-spa8000-fw-download>`.

xivo-patton-6.10
----------------

:v1.1: Plugin added which provision FXS patton gateways (SN411X and SN43XX)


xivo-patton-SN4120-6.10
-----------------------

:v1.2: Fix plugin installation (install rule for SN4120 firmware was wrong)
:v1.1: Plugin added which provision BRI patton gateway (SN4120)


xivo-polycom-4.0.11
-------------------

.. note:: Replaces xivo-polycom-4.0.9 plugin: *this plugin is required* for Polycom devices with MAC address in the range 64167F (instead of 0004F2)

:v2.2: fix URL for Updater file download
:v2.1: DND feature disabled
:v1.9: integrate XiVOCC required template configuration


xivo-polycom-5.4.3
------------------

:v2.1: DND feature disabled
:v1.8: integrate XiVOCC required template configuration


xivo-snom-8.7.5.35
------------------

:v2.4: Internal changes.
:v2.3: Change provisioning of :ref:`login_pause_funckeys` keys to support Wrapup indication.
:v2.2: Add support to SIP Auto-Answer Header (needed for CTI Transfer in Polaris)
:v2.1: DND feature disabled
:v1.9: update to use the new language pack
:v1.8: correction for provisioning if there is no DST


xivo-snom-8.9.3.80
------------------

.. note:: Replaces xivo-snom-8.9.3.60 plugin

:v2.6: Support for fw 8.9.3.80 and use check-sync NOTIFY event with reboot=false.
:v2.5: Change parameter to fix Switchboard answer
:v2.4: Change provisioning of :ref:`login_pause_funckeys` keys to support Wrapup indication.
:v2.3: Add support for D712 and upgrade firmware for 7XX serie.
:v2.2: Add support to SIP Auto-Answer Header (needed for CTI Transfer in Polaris)
:v2.1: DND feature disabled
:v1.8: correction for provisioning if there is no DST


xivo-yealink-v70
----------------

:v2.1.2: Replace broken firmware link
:v2.1: DND feature disabled


xivo-yealink-v72
----------------

:v2.1: DND feature disabled


xivo-yealink-v73
----------------

:v2.1: DND feature disabled


xivo-yealink-v81
----------------

.. note:: Replaces xivo-yealink-v80 plugin

:v2.5.0: add XiVO logo for T46G/S and T48G/S
:v2.4.3: fix firmware downloading
:v2.4.2: add provisioning of T27G in firmware v81
:v2.4.1: add provisioning of W52P in firmware v81
:v2.3: switch to default Yealink french translation to fix duplicate label for T48X

   .. note:: It leads to several small differences in the labels, for example:
         
      * *Attente* is changed to *Mtr Att*
      * *Journal* is changed to *Historique*
      * *Annuaire* is changed to *Répertoires*

:v2.2: add support to expansion module EXP40 for T46/T48 **S**
:v2.1: new version of Yealink plugin with support for T4XS family and T23G

   .. warning::
      To install firmware for T4XS family, the 'unrar' executable must be
      present on the host system. If it is not, you'll have a runtime error while
      trying to install any firmware.

      Starting from Five.08 and 2017.08 unrar-free and unar will be installed as
      a dependency of xivo-fetchfw package.

      Otherwise you need to install them manually :

        .. code-block:: bash

           apt-get update
           apt-get install unrar-free unar

      Some older versions of unrar-free fail to decompress the firmware package.
      In that case you will need to `enable non-free packages <https://wiki.debian.org/SourcesList#Example_sources.list>`_
      in debian and install unrar-nonfree :

        .. code-block:: bash

           apt-get update
           apt-get install unrar


xivo-yealink-v84
----------------

.. note:: Replaces xivo-yealink-v81 plugin for Yealink T4XS models (and others, see plugin info)

:v1.2: fix answering of second call from UC Assistant or CC Agent
:v1.1: first version of the plugin for fw v84

   .. note:: What's new from v81 plugin:
      * Small differences for some french labels, for example:

        * *Mtr Att* is changed to *Attente*
        * *Historique* is changed to *Récents*
        * *Répertoires* is changed to *Annuaire*

      * New *Station name* with first line display name and number
      * XiVO logo for T46S and T48S
      * Admin default password was changed
