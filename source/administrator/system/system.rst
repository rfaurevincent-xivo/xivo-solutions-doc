******
System
******

.. toctree::
   :maxdepth: 2

   DHCP Server <dhcp/dhcp>
   Mail <mail/mail>
   Network <network/network>
   database
   backup_restore
   cli_tools/cli_tools
   https_certificate
   asterisk_http
   configuration_files
   consul
   log_files
   nginx
   ntp
   proxy
   service_discovery
   service_authentication/service_authentication
   xivo-auth <xivo-auth/xivo-auth>
   xivo-confd <xivo-confd/xivo-confd>
   xivo-confgend <xivo-confgend/xivo-confgend>
   xivo-dird <xivo-dird/xivo-dird>
   xivo-dird-phoned <xivo-dird-phoned>
   xivo-purge-db <purge_logs>
   xivo-service <service>
   xivo-upgrade <xivo_upgrade_script>
   xivo-sysconfd <xivo-sysconfd>
