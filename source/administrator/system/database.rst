.. _database_container:

********
Database
********

.. _database_creation:

Creation and Initialization
===========================

Starting from the Callisto version, the database on the **XiVO PBX** and **all mediaservers** is started inside a docker container based on custom image.
On first startup the database will be initialized and the required structure and data will be created inside a host mounted folder ``/var/lib/postgresql/11/main/``.

.. _database_configuration:

Custom database configuration
=============================

The database image contains a default postgres configuration and some specific defaults required by our application. The postgres default configuration is located in ``/var/lib/postgresql/11/main/postgresql.conf`` and our custom defaults are in ``/var/lib/postgresql/11/main/conf.d/00-xivo-default.conf``. Do not change these files! If you need to change some parameters, create another file in ``/var/lib/postgresql/11/main/conf.d/`` prefixed with a number like ``01`` or upper and with a ``.conf`` extension. This file will be loaded after all defaults and can override any parameter.

Sample configuration
********************

Here is an example to increase the default number of concurrent connection to the database:

``/var/lib/postgresql/11/main/conf.d/10-custom-max-connection.conf``::

  max_connections = 300

