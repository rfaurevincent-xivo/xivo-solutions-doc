*******************
Proxy Configuration
*******************

If you use XiVO behind an HTTP proxy, you must do a couple of manipulations for
it to work correctly.

.. warning:: We do not recomend to use ``http_proxy`` environment variable. It may
     break some services.
     Instead you should configure the proxy on a per service basis as described
     below.

apt
===

Create the :file:`/etc/apt/apt.conf.d/90proxy` file with the following content::

   Acquire::http::Proxy "http://domain\username:password@proxyip:proxyport";


dhcp-update
===========

*This step is needed if you use the DHCP server of the XiVO. Otherwise the DHCP configuration won't be correct.*

Proxy information is set via the :file:`/etc/xivo/dhcpd-update.conf` file.

Edit the file and look for the ``[proxy]`` section.


docker
======

When upgrading or installing XiVO it will attempt to download docker images.
For the proxy configuration, you need to create a systemd configuration file.
See Docker documentation: https://docs.docker.com/config/daemon/systemd/#httphttps-proxy


provd
=====

Proxy information is set via the :menuselection:`Configuration --> Provisioning --> General`
page.


wget
====

*This step is needed because this tool is used by xivo-upgrade script.*

Create the :file:`~/.wgetrc` file with the following content::

   use_proxy=yes
   http_proxy=http://username:password@proxyip:proxyport


xivo-fetchfw
============

*This step is not needed if you don't use xivo-fetchfw.*

Proxy information is set via the :file:`/etc/xivo/xivo-fetchfw.conf` file.

Edit the file and look for the ``[proxy]`` section.
