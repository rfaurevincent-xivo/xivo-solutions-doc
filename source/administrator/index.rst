
*********************
Administrator's Guide
*********************

In-depth documentation on administration of XiVO solution systems.

XiVO Administration
===================

.. toctree::
   :maxdepth: 2

   System Management <system/system>
   xivo/high_availability/high_availability

XiVOcc Administration
=====================

.. toctree::
   :maxdepth: 2

   System Configuration <xivocc/system_configuration>
   Applications Configuration <xivocc/configuration/index>
   Administration <xivocc/admin/admin>

Troubleshooting
===============

.. important::

   * When reading this section, keep in mind the :ref:`xivocc_architecture_and_flows` diagram.
   * If you want to troubleshoot your installation see :ref:`check_installation`
   * For desktop application see :ref:`desktop_troubleshoot`

.. toctree::
   :maxdepth: 1

   troubleshooting/troubleshooting
   troubleshooting/xuc/index
   troubleshooting/nginx/index
   XiVOcc Postgresql <troubleshooting/postgresql/index>
   troubleshooting/pack_reporting/index
