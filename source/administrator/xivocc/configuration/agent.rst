.. _agent_configuration:

**********************
CC Agent configuration
**********************

.. _agent_recording:

Recording
=========

Recording can be paused or started by an agent.

+---------------------------------------+------------------------------------------+
| Recording On                          | Recording Paused                         |
+=======================================+==========================================+
|                                       |                                          |
|   .. figure:: agent_recording_on.png  |  .. figure:: agent_recording_pause.png   |
|       :scale: 70%                     |      :scale: 70%                         |
|                                       |                                          |
+---------------------------------------+------------------------------------------+

This feature can be disabled by changing ``showRecordingControls`` option in file :file:`application.conf`.
You can also set the environnment variable ``SHOW_RECORDING_CONTROLS`` to false for your xucmgt container in :file:`/etc/docker/compose/custom.env` file. When disabled the recording status is not displayed any more


Activity (Queue) control
========================

By using the ``showQueueControls`` option in application.conf, you may allow an agent to enter or leave an activity.
You can also use ``SHOW_QUEUE_CONTROLS`` environment variable in :file:`/etc/docker/compose/custom.env` file.

.. figure:: ccagent_activities.png
    :scale: 90%

.. _hold_notification:

On hold notification 
====================

You can configure ``notifyOnHold`` (in seconds)) in application.conf, this option once set, will trigger a popup and a system notification to user if he has a call on hold for a long time.
You can also use ``NOTIFY_ON_HOLD`` environment variable in :file:`/etc/docker/compose/custom.env` file.

Pause Cause and Status
======================

You can configure XiVO to have the following scenario:

* The agent person leaves temporarily his office (lunch, break, ...)
* He sets his presence in the CCAgent to the according state
* The agent will be automatically set in pause and his phone will not ring from
  queues
* He comes back to his office and set his presence to 'Available'
* The pause will be automatically cancelled

.. figure:: ccagent_states.png
    :scale: 90%

By default the pause action from the agent cannot be specified with a specific cause such as Lunch Time, or Tea Time. To be able to use a specific cause,
you will have to define new Presences in the cti server configuration.

.. figure:: agentpausecauses.png
    :scale: 90%

You define presences with action **Activate pause to all queue** to **true**, for not ready causes,
and you must have one presence defined with an action **Disable pause to all queue** to be able to go back to not ready.
When this presences are defined, you must restart the xuc server to be able to use them in ccagent, these presences will also be
automatically available in :ref:`CCmanager <ccmanager>` and new real time counters will be calculated.

+---------------------------------+---------------------------------+
| Presence from ready to pause    |   Presence from pause to ready  |
+=================================+=================================+
|                                 |                                 |
|   .. figure:: readytopause.png  |  .. figure:: pausetoready.png   |
|       :scale: 70%               |      :scale: 70%                |
|                                 |                                 |
+---------------------------------+---------------------------------+

.. _screen_popup:

Screen Popup
============

It is possible to display customer information in an external web application using Xivo :ref:`sheet <custom-call-form>` mecanism.

* :menuselection:`Services > CTI Server > Sheets > Models`:

  * Tab *General Settings*: Give a name
  * Tab *Sheet*: You must define a sheet with at least ``folderNumber`` and ``popupUrl`` fields set:

    * ``folderNumber`` (MANDATORY)

      * field type = ``text``
      * It has to be defined. Can be calculated or use a default value not equal to "-"
      * Note: You could leave "empty" using a whitespace (in hexadecimal: %20)

    * ``popupUrl`` (MANDATORY)

      * field type = ``text``
      * The url to open when call arrives : i.e. http://mycrm.com/customerInfo?folder= the folder number will be automatically
        appended at the end of the URL
      * Additionally to the existing xivo variables, you can also use here the following variables(only available in Web Agent and Desktop Agent):

        * ``{xuc-token}``: will be replaced by a token used for xuc websocket and rest api, for example ``http://mycrm.com/customerInfo?token={xuc-token}&folder=``
        * ``{xuc-username}``: will be replaced by the username of the logged on user, for example ``http://mycrm.com/customerInfo?username={xuc-username}&folder=``

    * ``multiTab`` (OPTIONAL)

      * field type = ``text``
      * set to the text ``true`` to open each popup in a new window.


* :menuselection:`Services > CTI Server > Sheets > Events`: Choose the right events for opening the URL (if you choose two events, url will opened twice etc.)

Example : Using the caller number to open a customer info web page

* Define ``folderNumber`` with any default value i.e. 123456
* Define ``popupUrl`` with a display value of http://mycrm.com/customerInfo?nb={xivo-calleridnum}&fn= when call arrives web page http://mycrm.com/customerInfo?nb=1050&fn=123456 will be displayed

.. figure:: example_xivo_sheet.png
    :scale: 90%

.. _login_pause_funckeys:

Login and Pause management using function keys
==============================================

You can configure Login or Pause keys on an agent phone. Their state will be synchronized with the state in *XiVO CC*
applications.

Behavior
--------

* **Login Key**:

  * change status of agent: login if it was logged out, and logout if it was logged in
  * LED of phone key will be updated accordingly as well as the status in *XiVO CC* applications (CCAgent...)
  * if you login/logout via *XiVO CC* applications (CCAgent...), status will be updated and phone key LED will be updated.

* **Pause key**:

  * change status of agent: pause if it was ready, ready if it was paused or in wrapup
  * LED of phone key will be updated accordingly as well as the status in *XiVO CC* applications (CCAgent...)
  * if you pause/unpause via *XiVO CC* applications (CCAgent...), status will be updated and phone key LED will be updated
  * **Wrapup**: if agent is on wrapup, the phone key will *blink*. If you press key while on wrapup, agent status will be changed to ready

    .. note::

      * The key blinks on Snom and Yealink phone sets. It doesn't blink on Polycom phone sets.
      * To be able to terminate Wrapup via the key on Snom phones you must use correct version of plugin (see
        :ref:`devices_releasenotes`).


Configuration
-------------

There are two types of customizable :ref:`function keys <function_keys>` that can be used

* Login: it will toggle login/logout of agent. There are two configuration patterns (see also below):

    * either ``***30<PHONE NUMBER>``: in this case it will ask for agent number and will then login the given agent on phone
      ``<PHONE NUMBER>``
    * or ``***30<PHONE NUMBER>*<AGENT NUMBER>``: in this case it will log agent ``<AGENT NUMBER>`` on phone ``<PHONE NUMBER>``

* Pause: it will toggle pause/unpause of agent (and will blink if agent is on Wrapup). Configuration pattern (see also below):

   * ``***34<PHONE NUMBER>`` : it will toggle pause/unpause of agent logged on phone ``<PHONE NUMBER>``


To use it you must:

#. On on XiVO PBX edit ``/etc/xivo-xuc.conf`` and change variables:

   * ``XUC_SERVER_IP`` to IP address of XivoCC
   * ``XUC_SERVER_PORT`` to port of XUC Server (default is 8090)

#. Configure function key on user (example with user 1000, 1001 and agent 8000 associated to user 1000):

   * Open :menuselection:`Services > IPBX > IPBX settings > Users`
   * Edit the user, open *Func Keys* tab and add keys like:
   * For Pause/Unpause agent logged on phone 1000 set:

      * :guilabel:`Type`: ``Customized``,
      * :guilabel:`Destination`: ``***341000``,
      * :guilabel:`Label`: ``Pause``,
      * :guilabel:`Supervision`: ``Enabled``

   * For Login/Logout agent on phone 1000 set:

      * :guilabel:`Type`: ``Customized``,
      * :guilabel:`Destination`: ``***301000``,
      * :guilabel:`Label`: ``Login``,
      * :guilabel:`Supervision`: ``Enabled``

   * For Login/Logout agent 8000 on phone 1001 set:

      * :guilabel:`Type`: ``Customized``,
      * :guilabel:`Destination`: ``***301001*8000``,
      * :guilabel:`Label`: ``LoginOn1001``,
      * :guilabel:`Supervision`: ``Enabled``


.. figure:: agent_funckey.png
    :scale: 90%
