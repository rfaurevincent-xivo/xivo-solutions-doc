.. _webassistant_configuration:

*************************
Web / Desktop Application
*************************

.. _webassistant_disable_webrtc:

Disabling WebRTC
================

WebRTC can be disabled globally by setting the ``DISABLE_WEBRTC`` environment varibale to ``true`` in :file:`/etc/docker/compose/custom.env` file.
