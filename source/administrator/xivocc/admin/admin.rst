.. _admin:

*********************
XiVOcc Administration
*********************

Start, stop or restart containers
=================================

Using the `xivocc-dcomp` script, you can control the run of the XiVO CC components:

  .. code-block:: bash

    xivocc-dcomp [command] [container]

List of commands:

* ``up -d`` - run containers
* ``stop`` - stop containers
* ``restart`` - restart containers

If you don't enter container name, the command applies on all containers.
Use container names without the ``xivocc_`` prefix and ``_1`` suffix.

.. _agent_states_after_xuc_restart:
.. warning:: Restarting xuc server with active calls may result in some agent's having incorrect state after the restart. Hang-up of such call will return agent into correct state.



Show status
===========

  .. code-block:: bash

    xivocc-dcomp ps

.. _admin_version:

Show containers and images versions
===================================

.. note:: Introduced in 2017.03.03 release.

Docker images are labelled with the exact version of the embedded application.

You can display the:

* Version of the running docker containers by typing:

  .. code-block:: bash

    xivocc-dcomp version

* Version of all docker containers (including stopped ones) by typing:

  .. code-block:: bash

    xivocc-dcomp version -a

* Version of docker images:

  .. code-block:: bash

    xivocc-dcomp version -i

For example :

.. code-block:: sh

  # xivocc-dcomp version
  NAMES                       VERSION
  xivocc_nginx_1              2019.02.00
  xivocc_xuc_1                2019.02.00
  xivocc_recording_server_1   2019.02.00
  xivocc_xucmgt_1             2019.02.00
  xivocc_config_mgt_1         2019.02.00
  xivocc_pack_reporting_1     2019.02.00
  xivocc_spagobi_1            2019.02.00
  xivocc_xivo_stats_1         2019.02.00
  xivocc_xivo_replic_1        2019.02.00
  xivocc_pgxivocc_1
  xivocc_elasticsearch_1
  xivocc_recording_rsync_1
  xivocc_kibana_volumes_1



This only applies to the following images:

* config_mgt
* nginx
* pack_reporting
* recording
* spagobi
* xivo_replic
* xivo_stats
* xuc
* xucmgt

.. note:: You can also use docker commands to display information about containers or images:

   * List all running containers with the exact version of application

     .. code-block:: sh

               $ docker ps --format 'table {{.Names}}\t{{.Image}}\t{{.Label "version"}}\t{{.Status}}'
               NAMES                       IMAGE                                         VERSION             STATUS
               xivocc_spagobi_1            xivoxc/spagobi:2017.03.latest                 2017.03.02          Up 14 hours
               xivocc_nginx_1              xivoxc/xivoxc_nginx:latest                                        Up 32 hours
               xivocc_xuc_1                xivoxc/xuc:2017.03.latest                     2017.03.02          Up 13 hours
               xivocc_recording_server_1   xivoxc/recording-server:2017.03.latest        2017.03.02          Up 32 hours
               xivocc_xivo_replic_1        xivoxc/xivo-db-replication:2017.03.latest     2017.03.02          Up 32 hours
               xivocc_config_mgt_1         xivoxc/config-mgt:2017.03.latest              2017.03.02          Up 32 hours
               xivocc_pack_reporting_1     xivoxc/pack-reporting:2017.03.latest          2017.03.02          Up 32 hours
               xivocc_xivo_stats_1         xivoxc/xivo-full-stats:2017.03.latest         2017.03.02          Up 32 hours
               xivocc_pgxivocc_1           xivoxc/pgxivocc:latest                                            Up 32 hours
               xivocc_xucmgt_1             xivoxc/xucmgt:2017.03.latest                  2017.03.02          Up 32 hours
               xivocc_elasticsearch_1      elasticsearch:1.7.2                                               Up 32 hours
               xivocc_fingerboard_1        xivoxc/fingerboard:latest                                         Up 32 hours
               xivocc_recording_rsync_1    xivoxc/recording-rsync:latest                                     Up 32 hours
               xivocc_kibana_volumes_1     xivoxc/kibana_volume:latest                                       Up 32 hours

   * You can also inspect an image or container to get it's exact version:

     .. code-block:: sh

                # Inspect an image
                $ docker inspect --format '{{(index .Config.Labels "version")}}' xivoxc/xuc:2017.03.latest
                2017.03.02

                # Inspect a running container
                $ docker inspect --format '{{(index .Config.Labels "version")}}' xivocc_xuc_1
                2017.03.02


Log
===

The log of each container can be found in the `/var/log/xivocc` directory.
Currently (it may change) the structure looks like this :

.. code-block:: sh

    /var/log/xivocc:
    ├── purge-reporting-database.log
    ├── recording-server
    │   ├── downloads.log
    │   └── recording-server.log
    ├── spagobi
    │   ├── Quartz.log
    │   ├── SpagoBIBirtReportEngine.log
    │   ├── SpagoBIChartEngine.log
    │   ├── SpagoBIJasperReports.log
    │   ├── SpagoBI.log
    │   ├── SpagoBIQbeEngineAudit.log
    │   ├── SpagoBIQbeEngine.log
    │   └── SpagoBITalendEngine.log
    ├── specific-stats.log
    ├── xivo-db-replication
    │   └── xivo-db-replication.log
    ├── xivo-full-stats
    │   └── xivo-full-stats.log
    ├── xuc
    │   ├── xuc_ami.log
    │   └── xuc.log
    └── xucmgt
        └── xucmgt.log


Backup
======

You may backup your databases by using a similar command as below, make sure you have enough space on disk.

.. code-block:: sh

    cd /var/backups
    mkdir xivocc
    cd xivocc

    docker run --rm --link xivocc_pgxivocc_1:db -v $(pwd):/backup -e PGPASSWORD=*** xivoxc/pgxivocc pg_dump -h db -U postgres --format=c -f /backup/xuc_rights_dump xuc_rights
    docker run --rm --link xivocc_pgxivocc_1:db -v $(pwd):/backup -e PGPASSWORD=*** xivoxc/pgxivocc pg_dump -h db -U postgres --format=c -f /backup/spagobi_dump spagobi
    docker run --rm --link xivocc_pgxivocc_1:db -v $(pwd):/backup -e PGPASSWORD=*** xivoxc/pgxivocc pg_dump -h db -U postgres --format=c -f /backup/recording_dump recording
    docker run --rm --link xivocc_pgxivocc_1:db -v $(pwd):/backup -e PGPASSWORD=*** xivoxc/pgxivocc pg_dump -h db -U postgres --format=c -f /backup/xivo_stats_dump xivo_stats


Restore
=======

You may restore a backup using a similar command (to be adapted)

.. code-block:: sh

    #Restore

    cd /var/backups/xivocc

    docker run --rm -it --link xivocc_pgxivocc_1:db -v $(pwd):/backup xivoxc/pgxivocc pg_restore -j 10 -h db -c -U postgres -d xivo_stats /backup/xivo_stats_dump
    docker run --rm -it --link xivocc_pgxivocc_1:db -v $(pwd):/backup xivoxc/pgxivocc pg_restore -j 10 -h db -c -U postgres -d spagobi /backup/spagobi_dump
    docker run --rm -it --link xivocc_pgxivocc_1:db -v $(pwd):/backup xivoxc/pgxivocc pg_restore -j 10 -h db -c -U postgres -d xuc_rights /backup/xuc_rights_dump
    docker run --rm -it --link xivocc_pgxivocc_1:db -v $(pwd):/backup xivoxc/pgxivocc pg_restore -j 10 -h db -c -U postgres -d recording /backup/recording_dump
