.. _uc-assistant:

************
UC Assistant
************

.. note:: This section describes the feature of the UC Assistant application.
   It is available as a web application from your Web Browser.
   It is also available as a *desktop application* with these additionnal features:

     * show integrated popup when receiving call
     * get keyboard shortcut to answer/hangup and make call using :ref:`Select2Call feature <dapp_global_key>`
     * :ref:`handle callto: and tel: links <dapp_call_url>`

   To install the *desktop application*, see :ref:`the desktop application installation <desktop-application-installation>`
   page.

**What is the XiVO UC Assistant ?**

The *XiVO UC Assistant* is a Web application that enables a user to:

* search contacts and show their presence, phone status
* make calls through physical phone or using WebRTC
* transfer incoming or outgoing calls
* access voicemail
* enable call forwarding and *Do Not Disturb* (aka DND)
* show history of calls


Login
=====

To login, you must have a user configured on the *XiVO PBX* with:

* XiVO Client enabled,
* Login, password and profile configured
* A configured line with a number

.. warning:: If a user tries to login without a line, an error message is displayed and
             user is redirected to the login page (this applies also to :ref:`desktop-application` )

.. figure:: errornoline.png
   :scale: 80%

.. note:: A **Remember me** option is available at prompt page to keep you signed in, when you want automatic login.


Search
======

You can use the search section to lookup for people in the company, results will display all information known for the user (phone numbers and email). You can either click on number to call, or click on copy button to put the number in your clipboard to paste it elsewhere.

.. figure:: search.png
   :scale: 100%

To enable this feature, you must configure the directories in the *XiVO PBX* as described in :ref:`directories` and :ref:`dird-integration-views`.


.. note:: Integration note: the *UC Assistant* support only the display of

  * 1 field for name (the one of type *name* in the directory display)
  * 3 numbers (the one of type *number* and the first two of type *callable*)
  * and 1 email


Forwarding Calls and DND
========================

From UC Assistant you can activate *Do Not Disturb* to block all incoming calls or forward call to any another number just by clicking on action button as seen on following screenshot:

.. figure:: action.png
   :scale: 100%

Once you choose an action, you just need to **apply** on clicking on associated button.

.. figure:: forward_dnd.png
   :scale: 100%

Action possibles are :

* Enable DND
* Disable DND
* Edit call forwarding (for both unconditional or on missed call only)

You know that all incoming calls will be rejected once you see the following logo in the header bar :

.. figure:: forward_dnd_logo.png
  :scale: 100%

All calls are forwarded once you see this following one :

.. figure:: forward_logo.png
   :scale: 100%

Finally, calls are forwarded only if you missed it when you see this one :

.. figure:: forward_na_logo.png
  :scale: 100%

.. note:: If calls are redirected, the forward number will be shown under your name.

  Nevertheless, there is a precedence, if DND mode is enabled and also call forwarding, calls will be rejected. If forward on miss call and all call forward are enabled, all calls will be forwarded to number configured for all call forwarding.


Favorites
=========

Click on the star to put a contact in its list of favorites.
Favorites must be configured in the *XiVO PBX* as described in :ref:`dird-favorites-configuration`.


.. _uc-assistant_personalcontacts:

Personal contacts
=================

From top-right hamburger menu, it is possible to display additional actions to handle you personal contacts.
You will be able to **create**, **delete all**, **import** and **export** personal contact that you will be either able to search from toolbar or find them in *favorites* panel if starred.

.. figure:: hamburger_menu.png
   :scale: 100%

Create a personal contact
-------------------------

Just fill wanted fields (such as name and number), click on star if you want this contact to be displayed in *favorites* panel.

.. warning:: It is not possible to have twice the same personal contact, at least one field must differ.

.. figure:: add_pc.png
   :scale: 75%

It's also possible to create a personal contact from call history by hovering a call item and so have pre-filled fields.

Edit a personal contact
-----------------------

To edit a personal contact, you should search it first, then you just hover it, and a pencil icon should appear as in following screen:

.. figure:: edit_pc.png
  :scale: 100%

Once clicked, you are redirected to edition pane where you just fill wanted fields.

Delete a personal contact
-------------------------

To delete a personal contact, you should edit it first, then you just need to click on trashcan icon :

.. figure:: delete_pc.png
  :scale: 100%

Once clicked, you are invited to confirm or not the deletion of this contact.


Import personal contacts
------------------------

From menu, you can upload a **.csv** file that contains all the data of your personal contacts. You can either use a file exported from this same interface or create yours.

Here are the list of available attributes of a personal contact:

* ``company``
* ``email``
* ``fax``
* ``firstname``
* ``lastname``
* ``mobile``
* ``number``

As an example here a csv file that can be imported

.. code::

  company,email,fax,firstname,lastname,mobile,number
  corp,jdoe@company.corp,3333,John,Doe,2222,1111

.. note:: File exported from previous *xivo-client* is also compatible with *UC assistant*.

Reverse lookup
--------------

By default, :ref:`reverse lookup <reverse_lookup>` is enabled for personal contact display on incoming calls. Configuration is set to display ``firstname`` and ``lastname`` if ``number`` or ``mobile`` matches an existing personal contact.

Phone integration
=================

The *UC Assistant* can integrate with the phone to :

* Call / Hangup
* Put on hold
* Do direct or indirect transfers :ref:`* <phone_integration_limitations>`
* Make a conference :ref:`* <phone_integration_limitations>`

\*) See :ref:`Known limitations <phone_integration_limitations>`

As these features are closely linked to the phone to work, you must check :ref:`phone_integration_support` and follow the :ref:`phone_integration_installation` page.


Once, you're phone is properly configured and you are connected as a user, you know that your using SIP phone once you see the following logo in the header bar :

.. figure:: fixed_logo.png
   :scale: 100%

On hold notifications
=====================

You can be notified if you forget a call in hold for a long time, see :ref:`configuration section <hold_notification>`.

.. figure:: user-hold-notification.png
   :scale: 90%


.. _uc-assistant_conferences:

Conferences
===========

When joining a conference, either as an attendee or an organizer, the *UC Assistant* will display specific informations about the conference you are joining.

.. figure:: conference_overview.png
   :scale: 60%

Conference information:
-----------------------
* The timer next to the conference name displays how long the conference has been running
* When hovering, the number of attendee will be displayed

Conference actions:
-------------------
.. figure:: conference_hover.png
   :scale: 60%

As an attendee, you can only:

* Exit a conference room by clicking the hangup button
* Put the conference on hold. Other attendees will not hear any hold music but will not be able to hear you neither you will be able to hear the conference room.

As an organizer, you will be able to mute and unmute all other attendee in the conference room.

Attendees information:
----------------------
.. figure:: conference_attendees_muted.png
   :scale: 60%

* Attendees name, number and timer are displayed below the conference name.
* Attendees are ordered by name with the exception of the first one which always reflect the current user
* Conference organizer are displayed in orange
* When an attendee is muted, an slashed microphone icon will be displayed next to its name
* When an attendee is talking, an orange speaker will be displayed next to its name

Attendee action:
----------------
.. figure:: conference_attendee_hover.png
   :scale: 60%

When hovering your own user, you can mute and unmute yourself.
Organizer can also:

* Mute and unmute any attendee
* Kick out an attendee. A message will be played to the kicked out attendee before leaving the conference.

WebRTC integration
==================

The *UC Assistant* can be used by users with WebRTC configuration, without physical phone.

For configuration and requirements, see :ref:`webrtc_requirements`.

You know that your using WebRTC once you see the following logo in the header bar :

.. figure:: webrtc_logo.png
   :scale: 100%


`*55` (echo test)
-----------------

To test your microphone and speaker, you may call the echo test application. After a welcome message, the application will
echo everything what you speak.

 1. Dial the `*55` number from your *Desktop Assistant*.
 2. You should hear the "Echo" announcement.
 3. After the announcement, you will hear back everything you say.
     If you hear what you are saying it means that your microphone and speakers are working.
 4. Press `#` or hangup to exit the echo test application.


.. _uc_webrtc_volume_indication:

Volume indication
-----------------

.. figure:: volume_meter.png

Two progress bars show the volume level of the speaker and the microphone. It certifies that the audio flow has been sent.

.. _uc_webrtc_ringing_device:

Ringing device selection
------------------------

When receiving a call, your computer will play a ringing sound however you can choose to play this sound on a separate audio device than the default selected by your operating system. For example, on a configuration with a headset, you may choose to have this device as your default device but you can override this selection to play the ringing sound on your computer instead.
You can choose the output device for the ringing sound by clicking on the top-right hamburger menu and then select the prefered device:

.. figure:: hamburger_menu.png
   :scale: 100%

.. figure:: ringing-device-selection.png
   :scale: 100%

This feature is only available when using a WebRTC line.


Experimental video call feature
===============================

.. note:: This experimental feature needs to be enabled in the configuration (see :ref:`configure_webrtc_video`).

When logged in as a user with a WebRTC line you can initiate and receive an audio/video call. The video call is possible
only from your favorite contacts or the directory search result through the video camera icon. When the called user
doesn't support video calls, call is established as audio only.


In the current implementation you can put the call on hold, go fullscreen, but for the moment you can have only one
video call at a time and you can't transfer the video call. Conference is not supported yet.

