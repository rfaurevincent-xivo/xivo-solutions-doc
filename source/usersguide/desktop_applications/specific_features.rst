*****************
Specific Features
*****************

This section lists the specific features per application available with the desktop application.

CC Agent
========

.. _dapp_agent_minimize:

Resize Window
-------------

Once *CC Agent* is launched through standalone application, a new button appears to be able to switch between a vertical
minimalist view and default one.

.. figure:: bar.png
   :scale: 100%

