.. _upgrade_cc:

*******
Upgrade
*******

Upgrading a *XiVO CC* is done by executing commands through a terminal on the
server.

.. note:: Downgrade is not supported

Overview
========

The upgrade consists of the following steps:

* switch/verify the version in the debian sources list
* update of the ``xivocc-installer`` package
* update of the Docker images

.. warning:: This upgrade procedure applies only to XiVO CC installed via the ``xivocc-installer`` package.

Preparing the upgrade
=====================

There are two cases:

#. :ref:`Upgrade to another LTS XiVO CC version <upgrade_to_lts_version_xcc>`,
#. :ref:`Upgrade to the latest Bugfix release <upgrade_latest_ltsbugfixrel_xcc>` of your current installed LTS version.


.. _upgrade_to_lts_version_xcc:

Upgrade to another LTS version
------------------------------

To upgrade to another XiVO Solution **LTS**:

#. Switch the debian sources to the targetted **LTS** version (it should be located in the file :file:`/etc/apt/sources.list.d/xivo-dist.list`). For example, to switch to Aldebaran LTS version::

    deb http://mirror.xivo.solutions/debian/ xivo-aldebaran main

#. **Read carefully the** :ref:`xivosolutions_release` starting from your current version to the version you target (read **even more
   carefully** the New features and Behavior changes between LTS)
#. **Check** the specific instructions and manual steps *from your current LTS to your targetted LTS* and all intermediate LTS: see :ref:`upgrade_lts_manual_steps`
#. **Check also** if you are in a specific setup that requires a :ref:`specific procedure <upgrade_specific_proc_xcc>`
#. And then upgrade, see `Upgrading`_


.. _upgrade_latest_ltsbugfixrel_xcc:

Upgrade to latest Bugfix release of an LTS version
--------------------------------------------------

.. important:: For version older than Five (2017.03), see `XiVO Five documentation <https://documentation.xivo.solutions/projects/five>`_

After the release of a *version* (e.g. *Polaris (2017.11)*) we may backport some bugfixes in this version.
We will then create a **subversion** (e.g. Polaris **.04** (2017.11 **.04**)) shipping these bugfixes.
These bugfix version does not contain any behavior change.

To upgrade to the **latest subversion** of your current installed *version* you need to:

#. **Read carefully the** :ref:`xivosolutions_release` starting from your installed version (e.g. Polaris.00) to the latest bugfix release (e.g. Polaris.04).
#. Verify that the debian sources list corresponds to your *installed LTS* (it should be located in the file :file:`/etc/apt/sources.list.d/xivo-dist.list`)
#. Verify that the :file:`/etc/docker/compose/factory.env` file has

   * ``XIVOCC_TAG=VERSION`` (where ``VERSION`` is your current installed *version* - e.g. *2017.11*)
   * and ``XIVOCC_DIST=latest``
#. And then upgrade, see `Upgrading`_


Upgrading
=========

After having prepared your upgrade (see above), you can upgrade:

#. When you have checked the sources.list you can upgrade with the following commands::

    apt-get update
    apt-get install xivocc-installer

#. If there is any change, you should accept the new :file:`docker-compose.yml` file. Then compare it with the old :file:`docker-compose.yml.dpkg-old` file and report in the new any specific configuration.
#. Then download the new docker images::

    xivocc-dcomp pull

#. And run the new containers (**Corresponding XiVO CC services will be restarted**)::

    xivocc-dcomp up -d --remove-orphans

.. note::
   Please, ensure your server date is correct before starting. If system date differs too much from correct date, you may get an authentication error preventing download of the docker images.

Post Upgrade
============

When finished:

* Check your upgrade through :ref:`check-list`.
* Check that all the services are in the correct version. Compare the output of ``xivocc-dcomp version`` with the table in :ref:`xivosolutions_release`


Manual steps for LTS upgrade
============================

See :ref:`upgrade_lts_manual_steps` in XiVO Upgrade page.


.. _upgrade_specific_proc_xcc:

Specific procedures
===================

.. toctree::
   :maxdepth: 2

   xivocc_old_pack_reporting_upgrade


Upgrade notes
=============

See :ref:`xivosolutions_release` for version specific informations.


