.. _check_installation:

***********************************
XiVOcc Installation Troubleshooting
***********************************

In order for the XiVOcc components to be fully functional, some customizations need to be done on the XiVOcc and the XiVO PBX.

This page can help to check that all the correct customization have been done by the installation package.

For the rest of this page we well make the following assumptions:
- XiVO PBX has the IP 192.168.0.1
- XiVO CC has the IP 192.168.0.2

.. important:: Refer to the :ref:`xivocc_architecture_and_flows` diagram.


Check XiVOcc Configuration
==========================

Check the prerequisites
-----------------------

- the OS must be Debian 9 (stretch), 64 bit,
- Docker must be installed,
- Docker-compose must be installed,
- the XiVO PBX must be reachable on the network.

.. check-list: installation#check-list

Check ntp installation
----------------------

The XiVO CC server and the XiVO server must be synchronized to the same source NTP source.

Check Logrotate configuration
-----------------------------

A file :file:`/etc/logrotate.hourly/docker-container` must be present which should log rotate files
:file:`/var/lib/docker/containers/*/*.log`

You can test it with ``logrotate -fv /etc/logrotate.hourly/docker-container``.
You should get some output and a new log file with suffix [CONTAINER ID]-json.log.1 should be created.
This file is compressed in next rotation cycle.


Check Docker compose
--------------------

- No alias for docker-compose should be defined. The following command should return "OK"::

    alias |grep -E 'docker-compose|dcomp' || echo "OK"

- The version of the docker images in the file :file:`/etc/docker/compose/docker-xivocc.yml` must be in the form ``${XIVOCC_TAG}.${XIVOCC_DIST}``
  and these variables must be set in the :file:`/etc/docker/compose/factory.env` file:

  .. code-block:: yaml

    ...
    xivo_stats:
        image: xivoxc/xivo-full-stats:${XIVOCC_TAG}.${XIVOCC_DIST}
    ...

    xuc:
        image: xivoxc/xuc:${XIVOCC_TAG}.${XIVOCC_DIST}
    ...


Check the services
------------------

The list of the services launched should look like :

.. code-block:: bash


    # xivocc-dcomp ps
               Name                         Command               State                        Ports
    ---------------------------------------------------------------------------------------------------------------------
    xivocc_elasticsearch_1      /docker-entrypoint.sh elas ...   Up       0.0.0.0:9200->9200/tcp, 0.0.0.0:9300->9300/tcp
    xivocc_fingerboard_1        /bin/sh -c /usr/bin/tail - ...   Up
    xivocc_kibana_volumes_1     /bin/sh -c /usr/bin/tail - ...   Up
    xivocc_nginx_1              nginx -g daemon off;             Up       443/tcp, 0.0.0.0:80->80/tcp
    xivocc_pack_reporting_1     /bin/sh -c echo            ...   Up
    xivocc_pgxivocc_1           /docker-entrypoint.sh postgres   Up       0.0.0.0:5443->5432/tcp
    xivocc_postgresvols_1       /bin/bash                        Exit 0
    xivocc_recording_server_1   bin/recording-server-docker      Up       0.0.0.0:9400->9000/tcp
    xivocc_reporting_rsync_1    /usr/local/sbin/run-rsync.sh     Up       0.0.0.0:873->873/tcp
    xivocc_spagobi_1            /bin/sh -c /root/start.sh        Up       0.0.0.0:9500->8080/tcp
    xivocc_timezone_1           /bin/bash                        Exit 0
    xivocc_xivo_replic_1        /usr/local/bin/start.sh /o ...   Up
    xivocc_xivo_stats_1         /usr/local/bin/start.sh /o ...   Up
    xivocc_xivocclogs_1         /bin/bash                        Exit 0
    xivocc_xuc_1                bin/xuc_docker                   Up       0.0.0.0:8090->9000/tcp
    xivocc_xucmgt_1             bin/xucmgt_docker                Up       0.0.0.0:8070->9000/tcp


Check the XiVO PBX
==================

Check PostgreSQL configuration
------------------------------

- Postgresql has to be configured to listen on all interfaces.
  See `listen_addresses` in file :file:`/etc/postgresql/9.4/main/postgresql.conf`.
- Connection from the XiVO CC for user asterisk must be authorized.
  See file :file:`/etc/postgresql/9.1/main/pg_hba.conf` which must contain a line::

   host asterisk all 192.168.0.2/32 md5
- A user `stats` must exists. Use command ``\dg`` in psql.


Check AMI configuration
------------------------

* A `xuc` user must be configured in the file :file:`/etc/asterisk/manager.d/02-xivocc.conf`
* The command::

   asterisk -rx "manager show user xuc"

 must show the user.

.. _cel:

CEL Configuration
-----------------

The correct events must be activated in the file :file:`/etc/asterisk/cel.conf`:

.. code-block:: ini

   [general]
   enable = yes
   apps = dial,park,queue
   events = APP_START,CHAN_START,CHAN_END,ANSWER,HANGUP,BRIDGE_ENTER,BRIDGE_EXIT,USER_DEFINED,LINKEDID_END,HOLD,UNHOLD,BLINDTRANSFER,ATTENDEDTRANSFER

   [manager]
   enabled = yes


Check CTI configuration
-----------------------

In :menuselection:`Services --> IPBX --> Users` a user the must be created with the following parameters:

- CTI login : xuc
- CTI password : 0000
- Profile supervisor


Check WS configuration
----------------------

In :menuselection:`Configuration --> Web Services Access` a user must be created with the following parameters :

- Login : xivows
- Password : xivows
- Host : 192.168.0.2


Check ACD configuration
-----------------------

In :menuselection:`Services --> Ipbx --> Advanced configuration` make sure ``Multiqueues call stats sharing`` is checked.


Check the phone integration
----------------------------

Verify that the phone configuration where customized as detailed in :ref:`Required configuration for phone integration
<phone_integration_installation>`.


Check the recording
-------------------

The package ``xivocc-recording`` must be installed on XiVO PBX (see :ref:`recording_xpbx`) and configured (see
:ref:`recording_configuration`).

