*****************************
Upgrade Borealis to Callisto
*****************************

In this section is listed the manual steps to do when migrating from Borealis to Callisto.

.. warning:: Upgrade to Callisto:

    * Postgres database will be updated from 9.4 to version 11 during upgrade.
      Migration time will therefore be longer than usual.
    * Asterisk will be updated from 13 to version 16 during upgrade.
    * IAX trunks are no longer supported

Before Upgrade
==============

On XiVO PBX
-----------

* Postgres upgrade:

  * You MUST have enough free disk space for postgres upgrade.
  * The upgrade duration will depend on the size of the database (and mainly on the size of `cel` and `queue_log` tables).
    As an example, for a database with 9M cel and 1M queue_log, the upgrade took 4 hours (on a server with 4 CPUs and 4 Gb of RAM).
  * Asterisk database roles must correspond to the installed (old) XiVO version. Migration can fail if some table
    is owned by an unexpected role.
  * If there is any specific postgresql parameters (e.g. in files :file:`/etc/postgresql/9.4/postgresql.conf` or
    or :file:`/etc/postgresql/9.4/pg_ha.conf`) you MUST save them to be able to restore them after the upgrade
    (see note After Upgrade below).


After Upgrade
=============

On XiVO PBX
-----------

* Outcall migration to Route:

  #. at the end of the upgrade check if there was any error during outcall migration to route::

      xivo-dcomp logs -t db | grep '\[MIGRATE_OUTCALL\].*WARNING'
  #. if yes, follow our :ref:`migration guide <callisto_route_upgrade_guide>`.

* Postgresql: postgresql service now runs inside a container. During upgrade the database was re-imported
  **BUT NOT** the postgresql configuration (i.e. parameters in file :file:`postgresql.conf`). If you have any
  specific parameters you should add them to the postgresql container configuration as documented in the
  :ref:`database_configuration` section.

* High availibility: If you have XiVO CC installed, follow the :ref:`ha_interconnection_with_cc` procedure
  to install DB Replic on the slave XiVO.

* Asterisk HTTP server configuration **security warning**:

  .. warning:: **Security warning:** Asterisk HTTP server (used for WebRTC and xivo-outcall) was reconfigured
     to accept websocket connections from outside. See the :ref:`asterisk_http_server` documentation
     and ensure that the configuration is secure.

* XDS: if you had an XDS installation you MUST remove SIP trunks added for intra-mds routing
  (that is the SIP trunks named `default`, and `mdsX` - e.g. `mds0`, `mds1` ...).
  These trunks are now generated automatically.
