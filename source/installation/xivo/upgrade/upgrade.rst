.. _upgrade:

*******
Upgrade
*******

Upgrading a *XiVO PBX* is done by executing commands through a terminal on the
server.

.. note:: Downgrade is not supported

Overview
========

The upgrade consists of the following steps:

* switch the version via ``xivo-dist`` utility
* upgrade via the ``xivo-upgrade`` utility: it will upgrade the system (Debian packages) and the *XiVO PBX* packages

.. warning:: The following applies to *XiVO PBX* **>= 2016.03**. For older version, see :ref:`version_specific_upgrade` section.


Preparing the upgrade
=====================

There are two cases:

#. :ref:`Upgrade to another LTS XiVO PBX version <upgrade_to_lts_version_xpbx>`,
#. :ref:`Upgrade to the latest Bugfix release <upgrade_latest_ltsbugfixrel_xpbx>` of your current installed LTS version.


.. _upgrade_to_lts_version_xpbx:

Upgrade to another LTS version
------------------------------

To prepare the upgrade you should:

#. Switch the sources to the new XiVO PBX **LTS** version with ``xivo-dist``, for example, to switch to Aldebaran LTS version::

    xivo-dist xivo-aldebaran

#. **Read carefully the** :ref:`xivosolutions_release` starting from your current version to the version you target (read **even more
   carefully** the New features and Behavior changes between LTS)
#. **Check** the specific instructions and manual steps *from your current LTS to your targetted LTS* and all intermediate LTS: see :ref:`upgrade_lts_manual_steps`
#. **Check also** if you are in a specific setup that requires a :ref:`specific procedure <upgrade_specific_proc>`
   to be followed (e.g. :ref:`upgrading-a-cluster`).
#. And then upgrade, see `Upgrading`_


.. _upgrade_latest_ltsbugfixrel_xpbx:

Upgrade to latest Bugfix release of an LTS version
--------------------------------------------------

After the release of an **LTS** *version* (e.g. *Polaris*) we may backport some bugfixes in this version.
We will then create a **subversion** (e.g. Polaris **.04**) shipping these bugfixes.
These bugfix version does not contain any behavior change.

To upgrade to the **latest subversion** of your current installed *version* you need to:

#. **Read carefully the** :ref:`xivosolutions_release` starting from your installed version (e.g. Polaris.00) to the latest bugfix release (e.g. Polaris.04).
#. Verify that the debian sources list corresponds to your *installed LTS* or refix it, for example for Polaris::

    xivo-dist xivo-polaris

#. And then upgrade, see `Upgrading`_


Upgrading
=========

.. note:: About `xivo-upgrade` script usage see :ref:`xivo-upgrade_script`

After having prepared your upgrade (see above), you can upgrade:

#. When ready, launch the upgrade process. **All XiVO PBX services will be stopped during the process**::

    xivo-upgrade

#. Verify that the docker services were downloaded::

    xivo-dcomp pull

#. Upgrade the docker services::

    xivo-dcomp up -d --remove-orphans


Post Upgrade
============

When finished:

* Check that all services are running::

   xivo-service status all

* Check that all the docker services are in the correct version. Compare the output of ``xivo-dcomp version`` with the table in :ref:`xivosolutions_release`
* Check that services are correctly working like SIP registration, ISDN link status,
  internal/incoming/outgoing calls, XiVO Client connections etc.


.. _upgrade_lts_manual_steps:

Manual steps for LTS upgrade
============================

.. toctree::
   :maxdepth: 1

   upgrade_from_five_to_polaris
   upgrade_from_polaris_to_aldebaran
   upgrade_from_aldebaran_to_borealis
   upgrade_from_borealis_to_callisto


.. _upgrade_specific_proc:

Specific procedures
===================

.. toctree::
   :maxdepth: 1

   cluster
   migrate_i386_to_amd64
   asterisk_latest
   xivocc_recording
   debian_9_stretch_upgrade_notes
   callisto_outcall_to_route_migration_guide


.. _version_specific_upgrade:

Version-specific upgrade procedures
===================================

.. note:: If your *XiVO PBX* is **below 2016.03** you have first to :ref:`switch-to-xivo.solutions` mirrors.

.. toctree::
   :maxdepth: 2

   switch_xivosolutions
   other_version_specific


Upgrading to/from an archive version
------------------------------------

.. toctree::
   :maxdepth: 1

   archives


Upgrade Notes
=============

See :ref:`xivosolutions_release` for version specific informations.

