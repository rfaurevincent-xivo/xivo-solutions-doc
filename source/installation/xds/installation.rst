**************
Installing XDS
**************

The XDS architecture has the following components:

* XiVO
* Media Server (MDS) (one or more)

An XDS needs also:

* a Reporting Server for the centralized call history,
* a CTI Server for the UC features.

This page will guide you through:

#. the configuration of the XiVO (see :ref:`xds_xivo-configuration` section)
#. the installation and configuration of the MDS (see :ref:`xds_mds-configuration` section)
#. and the configuration of the CC (Reporting and CTI Server) (see :ref:`xds_cc-configuration` section)


Requirements
============

Before starting you need to have 3 servers.
Here's a table summarizing what we are installing. Replace the IP by those you chose.

+-------------+-----------+--------------+----------------------+
| **Server**  | server1   | server2      | server3              |
+-------------+-----------+--------------+----------------------+
| **Role**    | XiVO      | Media Server | Reporting/CTI Server |
+-------------+-----------+--------------+----------------------+
| **Name**    | mds0      | mds1         | cc                   |
+-------------+-----------+--------------+----------------------+
| **IP Data** | 10.32.0.1 | 10.32.0.101  | 10.32.0.9            |
+-------------+-----------+--------------+----------------------+
| **IP VoIP** | 10.32.5.1 | 10.32.5.101  | 10.32.5.9            |
+-------------+-----------+--------------+----------------------+

.. _xds_xivo-configuration:

XiVO Configuration
==================

On *server1*:

* install XiVO (see :ref:`install`).
* pass the Wizard


.. _xds_xivo-define_media_server:

Define Media Servers
--------------------

.. note:: Here we define our Media Servers (MDS) names and VoIP IP address.

In XiVO webi,

#. Go to :menuselection:`Configuration -> Management -> Media Servers`
#. Add a line per Media Server (MDS) (below an example for mds1):

   #. :guilabel:`Name`: mdsX (e.g. mds1)
   #. :guilabel:`Displayed Name`: Media Server X (e.g. Media Server 1)
   #. :guilabel:`IP VoIP`: <VoIP IP of mdsX> (e.g. 10.32.5.101) - *note:* the VoIP streams between XiVO and mdsX will go through this IP

Once you define a media server, you will be able to create local SIP trunks that exist only there.
The location can be set in :menuselection:`tab General --> Media server` in the SIP trunk configuration.

Define Media Servers for Provisionning
--------------------------------------

.. note:: Here we configure the Media Servers (MDS) for the phones.

In XiVO webi

#. Go to :menuselection:`Configuration -> Provisioning -> Template Line`
#. Create a template line per MDS (below the example for mds1):

   #. :guilabel:`Unique name`: <mdsX> (e.g. mds1) - *note:* it **must be** the same name as the one defined in section :ref:`xds_xivo-define_media_server`
   #. :guilabel:`Displayed Name`: <Media Server X> (e.g. Media Server 1)
   #. :guilabel:`Registrar Main`: <VoIP IP of mdsX> (e.g. 10.32.5.101)
   #. :guilabel:`Proxy Main`: <VoIP IP of mdsX> (e.g. 10.32.5.101)


.. _xds_mds-configuration:

Media Server Configuration
==========================

Requirements
------------

On *server2* install a **Debian 9** with:
   
   * ``amd64`` architecture,
   * ``en_US.UTF-8`` locale,
   * ``ext4`` filesystem
   * a hostname correctly set (files :file:`/etc/hosts` and :file:`/etc/hostname` must be coherent).

Before installing the MDS you **have to** have added:

   * the MDS to the XiVO configuration (see :ref:`xds_xivo-define_media_server` section)


Installation
------------

.. important:: The MDS installer will ask you:

    * the XiVO Data IP Address
    * the Media Server you're installing (taken from the Media Server you declared at step :ref:`xds_xivo-define_media_server`)
    * the Media Server Data IP
    * the Reporting Server IP

To install the MDS, download the XiVO installation script:

.. code-block:: bash

   wget http://mirror.xivo.solutions/mds_install.sh
   chmod +x mds_install.sh

and run it:
   
.. important:: Use ``-a`` switch to chose **the same version** as your XiVO (mds0)

.. code-block:: bash

    ./mds_install.sh -a 2019.05-latest

When prompted:

* give the IP of XiVO (mds0): <XiVO Data IP> (e.g. 10.32.0.2)
* select the MDS you're installing: <mdsX> (e.g. mds1)
* enter the MDS Data IP: <mdsX Data IP> (e.g. 10.32.0.101)
* and finally the Reporting Server Data IP: <reporting Data IP) (e.g. 10.32.0.5)


Configuration
-------------

To finalize the MDS configuration you have to:

#. Create file :file:`/etc/asterisk/manager.d/02-xuc.conf` to add permission for Xuc Server to connect with:

   * ``secret`` must be the same as the secret for xuc user on XiVO,
   * ``permit`` to authorize the VoIP IP of the Reporting Server
   * e.g.:

      .. code-block:: bash

       cat > /etc/asterisk/manager.d/02-xuc.conf << EOF
       [xuc]
       secret = muq6IWgNU1Z
       deny=0.0.0.0/0.0.0.0
       permit=10.32.5.9/255.255.255.255
       read = system,call,log,verbose,command,agent,user,dtmf,originate,dialplan
       write = system,call,log,verbose,command,agent,user,dtmf,originate,dialplan
       writetimeout = 10000
       EOF

#. Restart the services::

    xivo-service restart all


Outgoing Call Configuration
===========================

Create the Provider Trunk
-------------------------

Add on the XiVO the trunk towards your provider (it can be an ISDN or SIP trunk).
When creating the trunk, select the Media Server on which it will be located.


Create Outgoing Call Rule
-------------------------

.. note:: This outgoing call rule will handle outgoing call from XDS to Provider.

In XiVO Webi:

#. Go to :menuselection:`Services -> IPBX -> Call Management -> Outgoing calls`
#. Create a route for XDS:

   #. Call pattern: X.
   #. Trunks: <your provider trunk>
   #. (after opening the advanced form) Caller ID: <main DID of the system>


.. _xds_cc-configuration:

XiVO CC Configuration
=====================

On *server3*:

* install a XiVO CC (see :ref:`ccinstallation`)
* configure it

Known Limitations
=================

Agent states after XUC restart
------------------------------

Restarting XUC server with active calls in XDS environment will result in having some agents in incorrect state.
Please see the note in :ref:`restarting <agent_states_after_xuc_restart>` XUC server with active calls.

