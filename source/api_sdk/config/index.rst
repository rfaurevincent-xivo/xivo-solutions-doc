###############################
XiVO Configuration MGT REST Api
###############################

This section describes the Config-MGT API.

In the following, all url are relative to the config-mgt base url and port. For example a relative URL ``/callback_lists`` is meant to be replaced by

::

  http://192.168.29.101:9100/configmgt/api/1.0/callback_lists

assuming that XiVO is available at ``192.168.29.101``

Authentication
==============

To use the config-mgt api you need to add an additional header in the HTTP Request. The header name is ``X-Auth-Token`` and its value must be the same as the ``authentication.token`` value in the ``application.conf`` of the config-mgt.

Example:

::

   curl -XGET -H "Content-Type: application/json" -H "Accept: application/json"
        -H 'X-Auth-Token: u@pf#41[gYHJm<]9N[a0iWDQQ7`e9k'  http://192.168.29.101:9100/configmgt/api/1.0/callback_lists

Profiles
========

Users can be given a certain *profile*. The profiles are:

* Administrator
* Supervisor
* Teacher

A profile defines:

#. an access right to some applications (CC Manager, Recording Server ...)
#. and also some rights inside the application.

These profiles are described in :ref:`profile_mgt` page.

Get all user's profiles
-----------------------

This api retrieves the profiles of all XiVO users having login.

**Description:**

:URL: ``/users``
:Method: ``GET``

----

**Example**
Query::

  curl -XGET -H "Content-Type: application/json" -H "Accept: application/json" -H 'X-Auth-Token: u@pf#41[gYHJm<]9N[a0iWDQQ7`e9k' 'http://localhost:9100/configmgt/api/1.0/users'

Response::

  [{"name":"Ménage","firstname":"Jean","login":"jmenage","profile":"teacher"},{"name":"Urbain","firstname":"Jocelyn","login":"jurbain","profile":"admin"}]

Get a user's profile and rights
-------------------------------

This api retrieves the profile and the associated right of a XiVO user having login.

**Description:**

:URL: ``/rights/user``
:Method: ``GET``
:Url parameters:
   :login: user's login name

----

**Example**
Query::

  curl -XGET -H "Content-Type: application/json" -H "Accept: application/json" -H 'X-Auth-Token: u@pf#41[gYHJm<]9N[a0iWDQQ7`e9k' 'http://localhost:9100/configmgt/api/1.0/rights/user/jbond'

Response::

  {"type":"supervisor","data":{"queueIds":[3,2,1,7],"groupIds":[3,1,2],"incallIds":[],"recordingAccess":true}}

Update user's profile or rights
-------------------------------

Updates the profile or the associated right of a XiVO user having login.

**Description:**

:URL: ``/rights/user``
:Method: ``POST``
:Url parameters:
   :login: user's login name
:Request body: Json object with field & value pair.

----

**Allowed field names:**

:type: The profile to set
:data: The rights to update

**Example**
Query::

  curl -XPOST -H "Content-Type: application/json" -H "Accept: application/json" -H 'X-Auth-Token: u@pf#41[gYHJm<]9N[a0iWDQQ7`e9k' 'http://localhost:9100/configmgt/api/1.0/rights/user/jbond' -d '{"type":"supervisor"}'

Delete user's profile
---------------------

Delete the profile associated to a XiVO user having login.

**Description:**

:URL: ``/rights/user``
:Method: ``DELETE``
:Url parameters:
   :login: user's login name

----

**Example**
Query::

  curl -XDELETE -H "Content-Type: application/json" -H "Accept: application/json" -H 'X-Auth-Token: u@pf#41[gYHJm<]9N[a0iWDQQ7`e9k' 'http://localhost:9100/configmgt/api/1.0/rights/user/jbond'


Callbacks
=========

The following API allow to define callbacks, a callback, is a shared note for agents in the same queue to know that he must call his customer before a deadline.
It is used in :ref:`CC Agent feature <agent>` existing in Xivo solutions.

A what so called `Callback` is in fact splitted in four distinct entities:
  * **Callback list**:  Container object used to define a set of callback requests
  * **Callback request**:  Core object that contains firstname, lastname, phone number... of the customer to call back
  * **Callback period**: The preferred interval of time in which the call should be performed
  * **Callback ticket**: Once callback request is taken by an agent, a ticket is created to sum up actions made on the request, like the status of the call and if the request is now closed or not.

More information on how to :ref:`Process Callbacks with CCAgent <callbacks_with_ccagent>`

Create Callbacks list
---------------------

Create a Callback list container for a queue

**Description:**

:URL: ``/callback_lists``
:Method: ``POST``
:Request body: Json object with field & value pair.

----

**Allowed field names:**

:name: The name of the list
:queueId: The queue to affect the callback requests

----

**Example**

Query::

  curl -XPOST -H "Content-Type: application/json" -H "Accept: application/json" -H 'X-Auth-Token: u@pf#41[gYHJm<]9N[a0iWDQQ7`e9k' 'http://localhost:9100/configmgt/api/1.0/callback_lists' -d '{"name":"newlist", "queueId":1}'


Response::

  {
      "callbacks": [],
      "name": "newlist",
      "queueId": 1,
      "uuid": "9d28d8fe-0548-4d45-aa08-9623ef69a04b"
  }

Get Callbacks list
------------------

List all the Callback list containers

**Description:**

:URL: ``/callback_lists``
:Method: ``GET``
:Url parameters:
   :withRequest: boolean to retrieve list if and only if it contains ongoing callback requests

----

**Example**

Query::

  curl -XGET -H "Content-Type: application/json" -H "Accept: application/json" -H 'X-Auth-Token: u@pf#41[gYHJm<]9N[a0iWDQQ7`e9k' 'http://localhost:9100/configmgt/api/1.0/callback_lists'


Response::

    [{"uuid":"fea963f5-1920-468c-b52a-93dc88791ba8","name":"Mine","queueId":2,"callbacks":
      [{"uuid":"edb734e7-9d8f-403d-8cf6-d42ecf9e48d7",
        "listUuid":"fea963f5-1920-468c-b52a-93dc88791ba8",
        "phoneNumber":"0230210092",
        "mobilePhoneNumber":"0689746321",
        "firstName":"John",
        "lastName":"Doe",
        "company":"MyCompany",
        "description":"Call back quickly",
        "preferredPeriodUuid":"31f91ef6-ebda-4e0d-a9fa-5ebd3da30951",
        "dueDate":"2017-09-27",
        "queueId":2,
        "clotured":false,
        "preferredPeriod":{"uuid":"31f91ef6-ebda-4e0d-a9fa-5ebd3da30951","name":"Toute la journée","periodStart":"09:00:00","periodEnd":"17:00:00","default":true}}
      ]},{"uuid":"75509ad3-3f81-40be-ad90-2c36d7a2c809","name":"another","queueId":2,"callbacks":[]}]

Delete Callbacks list
---------------------

Delete a Callback list container

**Description:**

:URL: ``/callback_lists``
:Method: ``DELETE``

----

**Example**

Query::

  curl -XDELETE -H "Content-Type: application/json" -H "Accept: application/json" -H 'X-Auth-Token: u@pf#41[gYHJm<]9N[a0iWDQQ7`e9k' 'http://localhost:9100/configmgt/api/1.0/callback_lists'


Response::

  {
      "callbacks": [],
      "name": "newlist",
      "queueId": 1,
      "uuid": "9d28d8fe-0548-4d45-aa08-9623ef69a04b"
  }

Import Callbacks requests
-------------------------

Import callback request in a Callback list container

**Description:**

:URL: ``/callback_lists/<listUuid>/callback_requests/csv``
:Method: ``POST``
:Url parameters:
   :listUuid: id of the callback list
:Request body: should be compliant with :ref:`following format <callbacks_with_ccmanager>`

----

**Example**

Query::

  curl -XPOST -H "Content-Type: text/plain" -H "Accept: text/plain" -H 'X-Auth-Token: u@pf#41[gYHJm<]9N[a0iWDQQ7`e9k' 'http://localhost:9100/configmgt/api/1.0//callback_lists/9d28d8fe-0548-4d45-aa08-9623ef69a04b/callback_requests/csv'-d '
      phoneNumber|mobilePhoneNumber|firstName|lastName|company|description|dueDate|period
      0230210092|0689746321|John|Doe|MyCompany|Call back quickly||
      0587963214|0789654123|Alice|O'Neill|YourSociety||2016-08-01|Afternoon'

Create Callback request
-----------------------

Create a single callback request in a callback list

**Description:**

:URL: ``/callback_lists/<listUuid>/callback_requests``
:Method: ``POST``
:Url parameters:
    :listUuid: id of the callback list
:Request body: Json object with field & value pair.

----

**Allowed field names:**

:phoneNumber: The number to call
:mobilePhoneNumber: Alternate number to call
:firstName: Contact first name (optional)
:lastName: Contact last name (optional)
:company: Contact company name (optional)
:description: Note displayed inside the callback for the agent (optional)
:dueDate: Deadline of the callback, using ISO format: YYYY-MM-DD
:period: Name of the period as defined in :ref:`callback list <callback_lists>`. (optional)

----

**Example**

Query::

  curl -XPOST -H "Content-Type: application/json" -H "Accept: application/json"  -H 'X-Auth-Token: u@pf#41[gYHJm<]9N[a0iWDQQ7`e9k' 'http://localhost:9100/configmgt/api/1.0//callback_lists/9d28d8fe-0548-4d45-aa08-9623ef69a04b/callback_requests' -d '
  {
      "company":"Cie",
      "phoneNumber":"0298765432",
      "mobilePhoneNumber":"0654321234",
      "firstName":"Jack"
  }'

Dynamic filters
---------------

A **dynamic filter** is just a JSON representation to create lite look-a-like SQL assertions.
It contains:

* **field**: Field to make your query on
* **operator**: Can be one of `=` , `!=` , `>` , `>=` , `<` , `<=` , `like` , `ilike` , `is null` , `is not null`
* **value**: value to filter on
* **order**: can be `ASC` or `DESC`


Find Callback request
---------------------

Finds a callback request using dynamic filters.

**Description:**

:URL: ``callback_requests/find``
:Method: ``POST``
:Request body: Json object with field & value pair.

----

**Allowed field names:**

:filters: List of dynamic filters
:offset: Distance between the beginning and the first result to retrieve, used for pagination (optional)
:limit: Max number of result to return (optional)

----

**Example**

Query::

  curl -XPOST -H "Content-Type: application/json" -H "Accept: application/json" -H 'X-Auth-Token: u@pf#41[gYHJm<]9N[a0iWDQQ7`e9k' 'http://localhost:9100/configmgt/api/1.0/callback_requests/find' -d '
  {
    "filters":[
      {"field":"phoneNumber", "operator":"=", "value":"1000"}
    ],
    "offset":0,
    "limit":100}'

Response::

  {
    "total":1,
    "list":[{
      "uuid":"7691e6c8-6ebc-4d8f-a41c-45c049ac0dd4",
      "listUuid":"fea963f5-1920-468c-b52a-93dc88791ba8",
      "phoneNumber":"1000",
      "mobilePhoneNumber":"2000",
      "preferredPeriodUuid":"a6119323-a793-4264-987b-c565ceac342b",
      "dueDate":"2018-07-05",
      "queueId":2,
      "clotured":false,
      "preferredPeriod":{
        "uuid":"a6119323-a793-4264-987b-c565ceac342b",
        "name":"Toute la journée",
        "periodStart":"09:00:00",
        "periodEnd":"17:00:00",
        "default":true
      }
    }]
  }

Agents
======

Get agent configuration
-------------------------

This api retrieves the agent configuration together with associated queues

**Description:**

:URL: ``/agent_config``
:Method: ``GET``
:Url parameters:
   :id: agent's id

----

**Example**

Query::

  curl -XGET -H "Content-Type: application/json" -H "Accept: application/json" -H 'X-Auth-Token: u@pf#41[gYHJm<]9N[a0iWDQQ7`e9k' 'http://localhost:9100/configmgt/api/1.0/agent_config/1'


Response::

    {"id":1,"firstname":"John","lastname":"Doe","number":"1001","context":"default","member": [
        {"queue_name":"queue1","queue_id":1,"interface":"Agent/1001","penalty":1,"commented":0,"usertype":"Agent","userid":1,"channel":"Agent","category":"Queue","position":1},
        {"queue_name":"queue2","queue_id":2,"interface":"Agent/1001","penalty":2,"commented":0,"usertype":"Agent","userid":1,"channel":"Agent","category":"Queue","position":1}],
    "numgroup":1,"userid":1}


Get all agent configurations list
---------------------------------

List all the agents together with associated queues

**Description:**

:URL: ``/agent_config``
:Method: ``GET``
:Url parameters:


----

**Example**

Query::

  curl -XGET -H "Content-Type: application/json" -H "Accept: application/json" -H 'X-Auth-Token: u@pf#41[gYHJm<]9N[a0iWDQQ7`e9k' 'http://localhost:9100/configmgt/api/1.0/agent_config'


Response::

    [{
    "id": 1,
    "firstname": "Agent",
    "lastname": "One",
    "number": "1001",
    "context": "default",
    "member": [{
        "queue_name": "queue1",
        "queue_id": 1,
        "interface": "Agent/1001",
        "penalty": 1,
        "commented": 0,
        "usertype": "Agent",
        "userid": 1,
        "channel": "Agent",
        "category": "Queue",
        "position": 1
      }],
    "numgroup": 1,"userid": 1
  },
  {
    "id": 2,
    "firstname": "Agent",
    "lastname": "Two",
    "number": "1002",
    "context": "default",
    "member": [{
        "queue_name": "queue2",
        "queue_id": 2,
        "interface": "Agent/1002",
        "penalty": 1,
        "commented": 0,
        "usertype": "Agent",
        "userid": 2,
        "channel": "Agent",
        "category": "Queue",
        "position": 1
      }],
    "numgroup": 1,"userid": 2 }]
