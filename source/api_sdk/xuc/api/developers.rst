.. developers_guide:

****************
Developers Guide
****************

Integration Principles
======================

* Include the Cti and Callback javascript API from the Xuc Server

::

   <script src="http://<xucserver>:<xucport>/assets/javascripts/shotgun.js" type="text/javascript"></script>
   <script src="http://<xucserver>:<xucport>/assets/javascripts/cti.js" type="text/javascript"></script>
   <script src="http://<xucserver>:<xucport>/assets/javascripts/callback.js" type="text/javascript"></script>
   <script src="http://<xucserver>:<xucport>/assets/javascripts/membership.js" type="text/javascript"></script>

* Include also the xc_webrtc and SIPml5 javascript APIs for the webRTC support:

::

   <script src="http://<xucserver>:<xucport>/assets/javascripts/xc_webrtc.js" type="text/javascript"></script>
   <script src="http://<xucserver>:<xucport>/assets/javascripts/SIPml-api.js" type="text/javascript"></script>

.. _cti_authentication:

* Connect to the Xuc server using new Authentication token (see :ref:`rest_authentication`)

::

   var wsurl = "ws://"+server+"/xuc/api/2.0/cti?token="+token;
   Cti.WebSocket.init(wsurl,username,phoneNumber);


* Connect to the Xuc server using XiVO client username and password (**DEPRECATED**)

::

   var wsurl = "ws://"+server+"/ctichannel?username="+username+"&amp;agentNumber="+phoneNumber+"&amp;password="+password;
   Cti.WebSocket.init(wsurl,username,phoneNumber);


* Setup event handlers to be notified on
   * Phone state changes
   * Agent state changes
   * Statistics
   * ...

* Eventually also webRTC handlers
   * general
   * register
   * incoming
   * outgoing

* Once web socket communication is established you are able to call XuC Cti javascript methods.
   * Place a call, log an agent ....

::

      ...
      $('#login_btn').click(function(event){
         Cti.loginAgent($('#agentPhoneNumber').val());
      });
      $('#logout_btn').click(function(event){
         Cti.logoutAgent();
      });
      $('#xuc_dial_btn').click(function(event){
         Cti.dial($("#xuc_destination").val());
      });
      ...

Sample Application
==================

A sample application is provided by the XuC server. This application allows to display events and using different methods exposed by the XuC

::

   http://<sucserver>:<xucport>/sample

.. figure:: sampleapp.png
    :scale: 90%

You may browse and use the ``sample.js`` javascript file as an example

* Calling Cti methods :

::

   .$('#xuc_login_btn').click(function(event) {
        Cti.loginAgent($('#xuc_agentPhoneNumber').val());
    });

    $('#xuc_logout_btn').click(function(event) {
        Cti.logoutAgent();
    });
    $('#xuc_pause_btn').click(function(event) {
        Cti.pauseAgent();
    });
    $('#xuc_unpause_btn').click(function(event) {
        Cti.unpauseAgent();
    });
    $('#xuc_subscribe_to_queue_stats_btn').click(function(event) {
        Cti.subscribeToQueueStats();
    });
    $('#xuc_answer_btn').click(function(event) {
        Cti.answer();
    });
    $('#xuc_hangup_btn').click(function(event) {
        Cti.hangup();
    });
    $('#xuc_login_btn').click(function(event) {
        Cti.loginAgent($('#xuc_agentPhoneNumber').val());
    });
    $('#xuc_logout_btn').click(function(event) {
        Cti.logoutAgent();
    });
    $('#xuc_togglelogin_btn').click(function(event) {
        Cti.toggleAgentLogin();
    });
    $('#xuc_pause_btn').click(function(event) {
        Cti.pauseAgent();
    });
    $('#xuc_unpause_btn').click(function(event) {
        Cti.unpauseAgent();
    });
    $('#xuc_subscribe_to_queue_stats_btn').click(function(event) {
        Cti.subscribeToQueueStats();
    });
    $('#xuc_answer_btn').click(function(event) {
        Cti.answer();
    });
    $('#xuc_hangup_btn').click(function(event) {
        Cti.hangup();
    });
    $('#xuc_get_agent_call_history').click(function() {
        Cti.getAgentCallHistory(7);
    });
    $('#xuc_get_user_call_history').click(function() {
        Cti.getUserCallHistory(7);
    });

   ..............

* Declaring events handlers :

::

        Cti.setHandler(Cti.MessageType.USERSTATUSES, usersStatusesHandler);
        Cti.setHandler(Cti.MessageType.USERSTATUSUPDATE, userStatusHandler);
        Cti.setHandler(Cti.MessageType.USERCONFIGUPDATE, userConfigHandler);
        Cti.setHandler(Cti.MessageType.LOGGEDON, loggedOnHandler);
        Cti.setHandler(Cti.MessageType.PHONESTATUSUPDATE, phoneStatusHandler);
        Cti.setHandler(Cti.MessageType.VOICEMAILSTATUSUPDATE, voiceMailStatusHandler);
        Cti.setHandler(Cti.MessageType.LINKSTATUSUPDATE, linkStatusHandler);
        Cti.setHandler(Cti.MessageType.QUEUESTATISTICS, queueStatisticsHandler);
        Cti.setHandler(Cti.MessageType.QUEUECONFIG, queueConfigHandler);
        Cti.setHandler(Cti.MessageType.QUEUELIST, queueConfigHandler);
        Cti.setHandler(Cti.MessageType.QUEUEMEMBER, queueMemberHandler);
        Cti.setHandler(Cti.MessageType.QUEUEMEMBERLIST, queueMemberHandler);
        Cti.setHandler(Cti.MessageType.DIRECTORYRESULT, directoryResultHandler);

        Cti.setHandler(Cti.MessageType.AGENTCONFIG, agentConfigHandler);
        Cti.setHandler(Cti.MessageType.AGENTLIST, agentConfigHandler);
        Cti.setHandler(Cti.MessageType.AGENTGROUPLIST, agentGroupConfigHandler);
        Cti.setHandler(Cti.MessageType.AGENTSTATEEVENT, agentStateEventHandler);
        Cti.setHandler(Cti.MessageType.AGENTERROR, agentErrorHandler);
        Cti.setHandler(Cti.MessageType.ERROR, errorHandler);
        Cti.setHandler(Cti.MessageType.AGENTDIRECTORY, agentDirectoryHandler);

        Cti.setHandler(Cti.MessageType.CONFERENCES, conferencesHandler);
        Cti.setHandler(Cti.MessageType.CALLHISTORY, callHistoryHandler);

        xc_webrtc.setHandler(xc_webrtc.MessageType.GENERAL, webRtcGeneralEventHandler);
        xc_webrtc.setHandler(xc_webrtc.MessageType.REGISTRATION, webRtcRegistrationEventHandler);
        xc_webrtc.setHandler(xc_webrtc.MessageType.INCOMING, webRtcIncomingEventHandler);
        xc_webrtc.setHandler(xc_webrtc.MessageType.OUTGOING, webRtcOutgoingEventHandler);

Debugging
=========

Cti features
------------

Cti events can be logged in the console if the ``Cti.debugMsg`` variable is set to ``true``, you can do it directly in
the developer tools console:
::

   Cti.debugMsg=true;

You'll then get send and received events in the console log (prefixed by ``S>>>`` and ``R<<<`` respectively):
::

   2016-11-23 14:48:59.180 S>>> {"claz":"web","command":"dial","destination":"111","variables":{}}
   2016-11-23 14:48:59.557 R<<< {"msgType":"PhoneStatusUpdate","ctiMessage":{"status":"CALLING"}}


