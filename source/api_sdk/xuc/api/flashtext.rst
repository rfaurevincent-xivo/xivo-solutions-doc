.. flashtext_api:

FlashText
=========

Allows user to send a text message to an other connected user, this message is delivered only 
if the destination user is connected. When destination user is not connected, the message is lost.

A sample of implementation is available in app/assets/javascripts/pages/sampleFlashText.js
and  app/views/sample/sampleFlashText.scala.html 

Handler
-------

* Handler on : Cti.MessageType.FLASHTEXTEVENT


Command
-------

Cti.sendFlashTextMessage(username, sequence_number, testmessage)

Example :

::
    
    Cti.sendFlashTextMessage("user", 24, "This is my message");

Messages received
-----------------

* FlashTextUserMessage

Received by the user when a message is sent

::

    {"from":{"username":"bruce"},"sequence":145,"message":"How are you Alice ?",
        "event":"FlashTextUserMessage"}

* FlashTextSendMessageAck

The message sent to the user can be delivered to the user, means the user is connected to the server, 
if the connected application is able to receive the message, message will be delivered.

::

    {"sequence":241,"event":"FlashTextSendMessageAck"}


* FlashTextSendMessageNack

The user is not connected to the server or user does not exists, message is lost. 


::

    {"sequence":28841,"event":"FlashTextSendMessageNack"}

