.. restapi:

########
Rest API
########


General form
============

   http://localhost:$xucport/xuc/api/$version/$method/$domain/$username/

   withHeaders(("Content-Type", "application/json"))

   * $xucport : Xuc port number (default 8090)
   * $version : 1.0 or 2.0
   * $method : See available methods below
   * $domain : Represents a connection site, can be anything
   * $username : XiVO client user username

Events
======

  Xuc post JSON formated events on URL ``eventUrl = "http://localhost:8090/xivo/1.0/event/avencall.com/dropbox/"``
  configured in /usr/share/xuc/application.conf


Phone Event Notification
------------------------

Related to a username, phone event is in message payload same structure as javascript :ref:`phoneevents`

::

    {
      "username":"alicej",
      "message":{
         "msgType":"PhoneEvent",
         "ctiMessage":{"eventType":"EventDialing","DN":"1058","otherDN":"3000","linkedId":"1447670380.34","uniqueId":"1447670380.34","userData":{"XIVO_USERID":"9"}}}}


.. _rest_authentication:

Obtain authentication token
===========================

Authentication is needed to use version 2.0 of the API

   POST http://localhost:$xucport/xuc/api/2.0/auth/login

::

   curl -XPOST -d '{"login": "<login>", "password": "<password>"}' -H "Content-Type: application/json"  http://localhost:8090/xuc/api/2.0/auth/login

Will retrieve an object

::

   {login: "<login>", token: "<token>"}

or an error

::

   {error:"<error_code>", message:"<error_message>"}

where error_code is one of:

* UserNotFound
* InvalidPassword
* InvalidJson
* UnhandledError

This token can then be used with the :ref:`CTI Authentication <cti_authentication>` and :ref:`rest_authentication_check`.

Obtain authentication token (SSO/Kerberos)
==========================================

   GET http://localhost:$xucport/xuc/api/2.0/auth/sso

::

   curl -XGET http://localhost:8090/xuc/api/2.0/auth/sso

Will retrieve an object

::

   {login: "<login>", token: "<token>"}

or an error

::

   {error:"<error_code>", message:"<error_message>"}

where error_code is one of:

* UserNotFound
* SsoAuthenticationFailed
* UnhandledError

This token can then be used with the :ref:`CTI Authentication <cti_authentication>` and :ref:`rest_authentication_check`.

Obtain authentication token (SSO/CAS)
==========================================

   GET http://localhost:$xucport/xuc/api/2.0/auth/cas?service=https://xucmgt.example.org&ticket=ST-11-Qsicgrh1mZ3dgoeOx7m6-af27d9025e0c

::

   curl -XGET http://localhost:8090/xuc/api/2.0/auth/cas?service=https://xucmgt.example.org&ticket=ST-11-Qsicgrh1mZ3dgoeOx7m6-af27d9025e0c

Will retrieve an object

::

   {login: "<login>", token: "<token>"}

or an error

::

   {error:"<error_code>", message:"<error_message>"}

where error_code is one of:

* UserNotFound: User was authenticated using SSO but the corresponding user does not exist on XiVO
* CasServerUrlNotSet: XiVOCC containers are not configured (see :ref:`CAS SSO Authentication <cas-sso-configuration>`)
* CasServerInvalidResponse: The CAS server returned an invalid response
* CasServerInvalidParameter: The Parameters sent to the CAS Server are invalid
* CasServerInvalidRequest: The Request to the CAS server is invalid
* CasServerInvalidTicketSpec: The ticket specification is invalid
* CasServerUnauthorizedServiceProxy: The CAS service proxy is not authorized
* CasServerInvalidProxyCallback: The CAS service proxy callback is invalid
* CasServerInvalidTicket: The ticket is invalid (probably expired or defined for another service)
* CasServerInvalidService: The service is invalid
* CasServerInternalError: CAS Server internal error
* UnhandledError

This token can then be used with the :ref:`CTI Authentication <cti_authentication>` and :ref:`rest_authentication_check`.

.. _rest_authentication_check:

Check authentication token
==========================

You can check the validity of a token using the following web service with an Authorization header set to 'Bearer <token>'

   GET http://localhost:$xucport/xuc/api/2.0/auth/check

::

   curl -X GET -H "Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJsb2dpbiI6InZtYWNoaW4iLCJleHBpcmVzIjoxNDg5OTk4Mzg2fQ.R6bzc5fBRs74l7ZBdLVjSIbNx3j3e07mV6mAX4_sklI" "http://localhost:8090/xuc/api/2.0/auth/check"

Will retrieve an object with a refreshed token

::

   {login: "<login>", token: "<token>"}

or an error

::

   {error:"<error_code>", message:"<error_message>"}

where error_code is one of:

* InvalidToken
* InvalidJson
* BearerNotFound
* AuthorizationHeaderNotFound
* TokenExpired
* UnhandledError

Connection
==========

This api is deprecated all the method are now able to autoconnect the user.

   POST http://localhost:$xucport/xuc/api/1.0/connect/$domain/$username/

::

   {"password" : "password"}

    curl -XPOST -d '{"password":"<password>"}' -H "Content-Type: application/json"  http://localhost:8090/xuc/api/1.0/connect/avencall.com/<username>/

DND
===

   POST http://localhost:$xucport/xuc/api/1.0/dnd/$domain/$username/

::

   {"state" : [false|true]}


   curl -XPOST -d '{"state":false}' -H "Content-Type: application/json"  http://localhost:8090/xuc/api/1.0/dnd/avencall.com/<username>/

Dial
====
   POST http://localhost:$xucport/xuc/api/1.0/dial/$domain/$username/

::

   {"number" : "1101"}

   curl -XPOST -d '{"number":"<number>"}' -H "Content-Type: application/json"  http://localhost:8090/xuc/api/1.0/dial/avencall.com/<username>/

DialByUsername
==============
  POST http://localhost:$xucport/xuc/api/1.0/dialByUsername/$domain/$username/

::

{"username" : "john"}

curl -XPOST -d '{"username":"<username>"}' -H "Content-Type: application/json"  http://localhost:8090/xuc/api/1.0/dialByUsername/avencall.com/<username>/



DialFromQueue
=============
   POST http://localhost:$xucport/xuc/api/1.0/dialFromQueue/$domain/$username/

::

   {"destination":"1002","queueId":5,"callerIdName":"Thomas","callerIdNumber":"999999","variables":{"foo":"bar"}}

   curl -XPOST -d '{"destination":"1002","queueId":5,"callerIdName":"Thomas","callerIdNumber":"999999","variables":{"foo":"bar"}}' -H "Content-Type: application/json"  http://localhost:8090/xuc/api/1.0/dialFromQueue/avencall.com/<username>/

**Limitations:** Queue No Answer settings does not work - see :ref:`xivo-queues-no-answer`. Except: when there is no free Agent to queue (none attached, all Agents on pause or busy), then No answer settings work (but Fail does not).

.. note:: Line should be configured with enabled "Ring instead of On-Hold Music" enabled (on "Application: tab in queue configuration - see :ref:`xivo-queues`). Otherwise the queue will answers the call and the destination rings even if there are no agents available.

Phone number sanitization
-------------------------

Dial command automatically applies filters to the phone number provided to make it valid for Xivo. Especially, it removes invalid characters and handles properly different notations of international country code.

Some countries don't follow the international standard and actually keep the leading zero after the country code (e.g. Italy). Because of this, if the zero isn't surrounded by parenthesis, the filter keeps it [1]_.

.. [1] See `Redmine ticket #150 <http://redmine.avencall.com:3000/issues/150>`_

Forward
=======
All forward commands use the above payload

::

      {"state" : [true|false],
        "destination" : "1102")

Unconditionnal
--------------
   POST http://localhost:$xucport/xuc/api/1.0/uncForward/$domain/$username/

::

   curl -XPOST -d '{"state":true,"destination":"<destnb>"}' -H "Content-Type: application/json"  http://localhost:8090/xuc/api/1.0/uncForward/avencall.com/<username>/

On No Answer
------------
   POST http://localhost:$xucport/xuc/api/1.0/naForward/$domain/$username/

::

   curl -XPOST -d '{"state":true,"destination":"<destnb>"}' -H "Content-Type: application/json"  http://localhost:8090/xuc/api/1.0/naForward/avencall.com/<username>/

On Busy
-------
   POST http://localhost:$xucport/xuc/api/1.0/busyForward/$domain/$username/

::

   curl -XPOST -d '{"state":true,"destination":"<destnb>"}' -H "Content-Type: application/json"  http://localhost:8090/xuc/api/1.0/busyForward/avencall.com/<username>/

Personal Contacts
=================
To handle personal contacts of a user

Error management
----------------

If an error occurs while using API actions, error will always be raised with proper HTTP return code and will be wrapped in JSON object with following format

::

  {
    "error": <error_code>
    "message": <cause>
  }


Error codes
-----------

+------------------+------------------+-----------------------------------------------------+
| Error code       | HTTP header code | Possible cause                                      |
+==================+==================+=====================================================+
| InvalidContact   | 400              | Personal contact has bad format                     |
+------------------+------------------+-----------------------------------------------------+
| ContactNotFound  | 404              | Personal contact does not exist                     |
+------------------+------------------+-----------------------------------------------------+
| DuplicateContact | 409              | Personal contact already exists                     |
+------------------+------------------+-----------------------------------------------------+
| Unreachable      | 503              | Directory server is not reachable                   |
+------------------+------------------+-----------------------------------------------------+
| JsonParsingError | 500              | Personal Contact sent to API is not JSON valid      |
+------------------+------------------+-----------------------------------------------------+
| NotHandledError  | 500              | Error not covered in current implementation         |
+------------------+------------------+-----------------------------------------------------+

List
----

   GET http://localhost:$xucport/xuc/api/2.0/contact/personal

::

   curl -XGET -H "Content-Type: application/json" -H "X-Auth-Token: eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJsb2dpbiI6InZtYWNoaW4iLCJleHBpcmVzIjoxNDg5OTk4Mzg2fQ.R6bzc5fBRs74l7ZBdLVjSIbNx3j3e07mV6mAX4_sklI" http://localhost:8090/xuc/api/2.0/contact/personal

Will retrieve an object containing all personal contacts `entries` with associated values defined in `headers` (HTTP code 200)

::

   {
     "entries": [
         { "status": 0, "entry": [ "hawkeye", "pierce", "1002", "0761187406", "false"]},
         { "status": -2, "entry": [ "peter", "pan", "1004", "", "false"]}],
     "headers":
         ["Firstname", "Lastname", "Number", "Mobile", "Favorite"]
    }

Get
---

   GET http://localhost:$xucport/xuc/api/2.0/contact/personal/$contactId

::

   curl -XGET -H "Content-Type: application/json" -H "X-Auth-Token: eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJsb2dpbiI6InZtYWNoaW4iLCJleHBpcmVzIjoxNDg5OTk4Mzg2fQ.R6bzc5fBRs74l7ZBdLVjSIbNx3j3e07mV6mAX4_sklI" http://localhost:8090/xuc/api/2.0/contact/personal/28079dc0-2c6b-4332-9075-61da9229389f

Will retrieve an object containing a single personal contact (HTTP code 200)

::

    {
      "id":"28079dc0-2c6b-4332-9075-61da9229389f",
      "firstname":"doe",
      "lastname":"john",
      "number":"1111",
      "mobile":"2222",
      "fax":"3333",
      "email":"j.doe@my.corp",
      "company":"corp"
    }

Create
------

   POST http://localhost:$xucport/xuc/api/2.0/contact/personal

::

    {
      "firstname":"doe",
      "lastname":"john",
      "number":"1111",
      "mobile":"2222",
      "fax":"3333",
      "email":"j.doe@my.corp",
      "company":"corp"
    }

   curl -XPOST -H "Content-Type: application/json" -H "X-Auth-Token: eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJsb2dpbiI6InZtYWNoaW4iLCJleHBpcmVzIjoxNDg5OTk4Mzg2fQ.R6bzc5fBRs74l7ZBdLVjSIbNx3j3e07mV6mAX4_sklI" http://localhost:8090/xuc/api/2.0/contact/personal -d '{"lastname":"john","firstname":"doe","company":"corp","email":"j.doe@my.corp","number":"1111","mobile":"2222","fax":"3333"}'

Will create and return object containing a single personal contact with id (HTTP code 201)

::

    {
      "id":"28079dc0-2c6b-4332-9075-61da9229389f",
      "firstname":"doe",
      "lastname":"john",
      "number":"1111",
      "mobile":"2222",
      "fax":"3333",
      "email":"j.doe@my.corp",
      "company":"corp"
    }

Update
------

   PUT http://localhost:$xucport/xuc/api/2.0/contact/personal/$contactId

::

    {
      "firstname":"doe",
      "lastname":"john",
      "number":"1111",
      "mobile":"2222",
      "fax":"3333",
      "email":"j.doe@my.corp",
      "company":"corp"
    }

   curl -XPUT -H "Content-Type: application/json" -H "X-Auth-Token: eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJsb2dpbiI6InZtYWNoaW4iLCJleHBpcmVzIjoxNDg5OTk4Mzg2fQ.R6bzc5fBRs74l7ZBdLVjSIbNx3j3e07mV6mAX4_sklI" http://localhost:8090/xuc/api/2.0/contact/personal/28079dc0-2c6b-4332-9075-61da9229389f -d '{"lastname":"john","firstname":"doe","company":"corp","email":"j.doe@my.corp","number":"1111","mobile":"2222","fax":"3333"}'

Will update and return object containing single personal contact with id (HTTP code 200)

::

    {
      "id":"28079dc0-2c6b-4332-9075-61da9229389f",
      "firstname":"doe",
      "lastname":"john",
      "number":"1111",
      "mobile":"2222",
      "fax":"3333",
      "email":"j.doe@my.corp",
      "company":"corp"
    }

Delete
------

   DELETE http://localhost:$xucport/xuc/api/2.0/contact/personal/$contactId

::

   curl -XGET -H "Content-Type: application/json" -H "X-Auth-Token: eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJsb2dpbiI6InZtYWNoaW4iLCJleHBpcmVzIjoxNDg5OTk4Mzg2fQ.R6bzc5fBRs74l7ZBdLVjSIbNx3j3e07mV6mAX4_sklI" http://localhost:8090/xuc/api/2.0/contact/personal/28079dc0-2c6b-4332-9075-61da9229389f

Will return empty content (HTTP code 204)

Delete All
----------

   DELETE http://localhost:$xucport/xuc/api/2.0/contact/personal

::

   curl -XGET -H "Content-Type: application/json" -H "X-Auth-Token: eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJsb2dpbiI6InZtYWNoaW4iLCJleHBpcmVzIjoxNDg5OTk4Mzg2fQ.R6bzc5fBRs74l7ZBdLVjSIbNx3j3e07mV6mAX4_sklI" http://localhost:8090/xuc/api/2.0/contact/personal

Will return empty content (HTTP code 204)

Export
------

   GET http://localhost:$xucport/xuc/api/2.0/contact/export/personal

::

   curl -XGET -H "X-Auth-Token: eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJsb2dpbiI6InZtYWNoaW4iLCJleHBpcmVzIjoxNDg5OTk4Mzg2fQ.R6bzc5fBRs74l7ZBdLVjSIbNx3j3e07mV6mAX4_sklI" http://localhost:8090/xuc/api/2.0/contact/export/personal

Will return csv text content with UTF-8 encoding (HTTP code 200) containing all personal contacts of a user

::

  company,email,fax,firstname,lastname,mobile,number
  corp,jdoe@company.corp,3333,John,Doe,2222,1111

Import
------

  POST http://localhost:$xucport/xuc/api/2.0/contact/import/personal

.. note:: request needs to have plain csv as content with utf-8 charset

::

   company,email,fax,firstname,lastname,mobile,number
   corp,jdoe@company.corp,3333,John,Doe,2222,1111

   curl -XPOST -H "Content-Type: text/csv;charset=UTF-8" -H "X-Auth-Token: eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJsb2dpbiI6InZtYWNoaW4iLCJleHBpcmVzIjoxNDg5OTk4Mzg2fQ.R6bzc5fBRs74l7ZBdLVjSIbNx3j3e07mV6mAX4_sklI" http://localhost:8090/xuc/api/2.0/contact/import/personal -d 'company,email,fax,firstname,lastname,mobile,number\ncorp,jdoe@company.corp,3333,John,Doe,2222,1111'

Will create and return object containing result of success/failure for each personal contact inserted (HTTP code 201)

::

  {
    "created": [{
      "id": "e62ee672-f74a-498c-b138-86ac1b0ae429",
      "lastname": "Wayne"
    }, {
      "id": "d3a67f8e-3d8a-465a-9633-bde65a41f1bb",
      "lastname": "Hawking"
    }],
    "failed": [{
      "line": 3,
      "errors": ["too many fields"]
    }]
  }

Handshake
=========
Will repost all events on the configured URL

   POST http://localhost:$xucport/xuc/api/1.0/handshake/$domain/

Agent
=====

AgentLogin
----------
Log an agent on an extension

    POST http://$xuchost:$xucport/xuc/api/1.0/agentLogin/

::

    curl -XPOST -d '{"agentphonenumber":"<phone number>", "agentnumber":"<agent number>"}' -H "Content-Type: application/json"  http://localhost:8090/xuc/api/1.0/agentLogin/


AgentLogout
-----------
Logout un agent

    POST http://$xuchost:$xucport/xuc/api/1.0/agentLogout/

::

    curl -XPOST -d '{"phoneNumber":"<phoneNumber>"}' -H "Content-Type: application/json"  http://localhost:8090/xuc/api/1.0/agentLogout/

TogglePause
-----------
Change state of an agent, pause if ready, ready if on pause or on wrapup

    POST http://$xuchost:$xucport/xuc/api/1.0/togglePause/

::

    curl -XPOST -d '{"phoneNumber":"<phoneNumber>"}' -H "Content-Type: application/json"  http://localhost:8090/xuc/api/1.0/togglePause/

User Call History
=================
Get user call history


::

    curl -XGET -H "Content-Type: application/json"  http://localhost:8090/xuc/api/1.0/historyByUsername/<domain>/<username>?size=X


Answer

::

    [
      {"start":"2018-09-20 17:38:41","duration":"00:00:02","srcUsername":"bruce","dstUsername":"alicej","status":"emitted"},
      {"start":"2018-09-20 17:19:40","duration":"00:00:01","srcUsername":"bruce","dstUsername":"cquefia","status":"emitted"},
      {"start":"2018-09-20 17:15:00","duration":"00:00:18","srcUsername":"cquefia","dstUsername":"bruce","status":"missed"},
      {"start":"2018-09-20 17:14:16","duration":"00:00:11","srcUsername":"cquefia","dstUsername":"bruce","status":"missed"}
    ]


Qualifications
==============

To retrieve call qualification options and create call qualification answer.

Error management
----------------

If an error occurs while using API actions, error will always be raised with proper HTTP return code and will be wrapped in JSON object with following format

::

  {
    "error": <error_code>
    "message": <cause>
  }


Error codes
-----------

+------------------+------------------+-----------------------------------------------------+
| Error code       | HTTP header code | Possible cause                                      |
+==================+==================+=====================================================+
| JsonBodyNotFound | 400              | Qualification answer sent to API is not found       |
+------------------+------------------+-----------------------------------------------------+
| JsonErrorDecoding| 400              | Qualification answer sent to API is not JSON valid  |
+------------------+------------------+-----------------------------------------------------+
| Unreachable      | 503              | Config management server is not available           |
+------------------+------------------+-----------------------------------------------------+
| NotHandledError  | 500              | Error not covered in current implementation         |
+------------------+------------------+-----------------------------------------------------+

Get
----

   GET http://localhost:$xucport/xuc/api/2.0/call_qualification/queue/$queueId

::

   curl -XGET -H "Content-Type: application/json" -H "X-Auth-Token: eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJsb2dpbiI6InZtYWNoaW4iLCJleHBpcmVzIjoxNDg5OTk4Mzg2fQ.R6bzc5fBRs74l7ZBdLVjSIbNx3j3e07mV6mAX4_sklI" http://localhost:8090/xuc/api/2.0/call_qualification/queue/$queueId

Will retrieve a list of objects containing all qualifications and sub qualifications for a single queue (HTTP code 200)

::

    [
      {
        "id": 6,
            "name": "General Questions",
            "subQualifications": [
              { "id": 14, "name": "Common"},
              { "id": 15, "name": "Technical" }
            ]
      },
      {
        "id": 5,
            "name": "Support",
            "subQualifications": [
              { "id": 12, "name": "Technical" },
              { "id": 13, "name": "General" }
            ]
      }
    ]

Create
------

   POST http://localhost:$xucport/xuc/api/2.0/call_qualification

::

    {
      "sub_qualification_id": 1,
      "time": "2018-03-21 17:00:00",
      "callid": "callid1",
      "agent": 1,
      "queue": 1,
      "first_name": "john",
      "last_name": "doe",
      "comment": "some comment",
      "custom_data": "some custom data"
    }

   curl -XPOST -H "Content-Type: application/json" -H "X-Auth-Token: eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJsb2dpbiI6InZtYWNoaW4iLCJleHBpcmVzIjoxNDg5OTk4Mzg2fQ.R6bzc5fBRs74l7ZBdLVjSIbNx3j3e07mV6mAX4_sklI" http://localhost:8090/xuc/api/2.0/call_qualification -d -d '{"sub_qualification_id":1,"time":"2018-03-21 17:00:00","callid":"callid1","agent":1,"queue":1,"first_name":"john","last_name":"doe","comment":"some comment","custom_data":"some custom data"}'

Will create a call qualification answer and return id of the created call qualification answer (HTTP code 201)

Export
------

   GET http://localhost:$xucport/xuc/api/api/2.0/call_qualification/csv/$queueId/$from/$to

::

   curl -XGET -H "Content-Type: application/json" -H "X-Auth-Token: eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJsb2dpbiI6InZtYWNoaW4iLCJleHBpcmVzIjoxNDg5OTk4Mzg2fQ.R6bzc5fBRs74l7ZBdLVjSIbNx3j3e07mV6mAX4_sklI" http://localhost:8090/xuc/api/2.0/call_qualification/csv/$queueId/$from/$to

Will return csv text content with UTF-8 encoding (HTTP code 200) containing all call qualification answer of a queue

::

  sub_qualification_id,time,callid,agent,queue,first_name,last_name,comment,custom_data
  1,2018-03-21 17:00:00,callid1,1,1,john,doe,some comment,some custom data
