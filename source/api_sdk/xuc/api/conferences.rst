.. conferences_api:

***************
Conferences API
***************

This api is to manipulate voice conference room

You can refer to :ref:`conference_configuration` configuration for organizer feature

.. _conference_events:

Conference Events
=================

See associated :ref:`conference_methods`

Conference Event
----------------

- Cti.MessageType.CONFERENCEEVENT

Received when you enter or leave a conference room.

::

    {
      "eventType": "Join",
      "uniqueId": "1519658665.8",
      "conferenceNumber": "4001",
      "conferenceName": "conference_support",
      "participants": [
        {
          "index": 1,
          "callerIdName": "James Bond",
          "callerIdNum": "1002",
          "since": 0,
          "isTalking": false,
          "role": "User",
          "isMuted": false,
          "isMe": true,
        }
      ],
      "since": 0
    }

Fields description :

    * eventType: "Join" or "Leave"
    * uniqueId: channel / call unique id entering this conference (related to Phone events)
    * conferenceNumber: DN Number of the joined/left conference
    * conferenceName: Name of the joined/left conference
    * participants: array of participant
    * since: delay in seconds since the beginning of the conference
 

Conference Participant Event
----------------------------

- Cti.MessageType.CONFERENCEPARTICIPANTEVENT

Received when a participant enter, leave, or be updated in your conference room.

::

    {
      "eventType": "Update",
      "uniqueId": "1519658665.8",
      "conferenceNumber": "4001",
      "index": 1,
      "callerIdName": "James Bond",
      "callerIdNum": "1002",
      "since": 0,
      "isTalking": true,
      "role": "Organizer",
      "isMuted": false,
      "isMe": false
    }

Fields description :

    * eventType: "Join", "Leave" or "Update"
    * uniqueId: channel / call unique id entering this conference (related to Phone events)
    * conferenceNumber: DN Number of the joined/left conference
    * conferenceName: Name of the joined/left conference
    * index: position of the participant in the conference
    * callerIdName: Name of the participant
    * callerIdNum: DN Number of the participant
    * since: delay in seconds since the beginning of the conference
    * isTalking: true or false if participant is talking
    * role: participant role, either "User" or "Organizer"
    * isMuted: indicate if participant is muted
    * isMe: indicate if participant is the current user

.. _conference_command_error:

Conference Command Error Event
------------------------------

- Cti.MessageType.CONFERENCECOMMANDERROR

Received after a conference command (mute/unmute, muteme/unmuteme, muteall/unmuteall, kick) if an error is encountered while processing the command

::

    {
      "error": "NotAMember"
    }

The error code can be one of the following:

    * NotAMember: The current user is not a member of the given conference number.
    * NotOrganizer: The current user is not an organizer in the given conference number and cannot perform the command.
    * CannotKickOrganizer: You cannot kick an organizer out of a conference.
    * ParticipantNotFound: The targeted participant wasn't found.

.. _conference_methods:

Conference Methods
==================

Cti.conference()
----------------
Start a conference using phone set capabilities

Cti.conferenceMuteMe(conferenceNumber)
--------------------------------------
Mute the current user in the given conference.

See also `conference_command_error`_.

Cti.conferenceUnmuteMe(conferenceNumber)
----------------------------------------
Un-mute the current user in the given conference

See also `conference_command_error`_.

Cti.conferenceMuteAll(conferenceNumber)
---------------------------------------
Mute all attendees except current user in the given conference. This method is restricted to conference organizer so you must enter the conference with an organizer pin.

See also `conference_command_error`_.

Cti.conferenceUnmuteAll(conferenceNumber)
-----------------------------------------
Un-Mute all attendees in the given conference. This method is restricted to conference organizer so you must enter the conference with an organizer pin.

See also `conference_command_error`_.

Cti.conferenceMute(conferenceNumber, index)
-------------------------------------------
Mute an attendee by its index in the given conference. This method is restricted to conference organizer so you must enter the conference with an organizer pin.

See also `conference_command_error`_.

Cti.conferenceUnmute(conferenceNumber, index)
---------------------------------------------
Un-Mute an attendee by its index in the given conference. This method is restricted to conference organizer so you must enter the conference with an organizer pin.

See also `conference_command_error`_.

Cti.conferenceKick(conferenceNumber, index)
---------------------------------------------
Kick an attendee out of the given conference by its index. This method is restricted to conference organizer so you must enter the conference with an organizer pin.

See also `conference_command_error`_.

