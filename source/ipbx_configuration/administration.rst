************************
IPBX Configuration Guide
************************

.. toctree::
   :maxdepth: 2

   advanced_configuration/advanced_configuration
   boss_secretary_filter
   call_completion/call_completion
   callright/callright
   cel/cel
   conference_room/conference_room
   ctiserver/ctiserver
   Customer info <customerinfo/customerinfo>
   devices/devices
   directories/directories
   directed_pickup/directed_pickup
   entities/entities
   fax/fax
   graphics/graphics
   Groups <group/group>
   Group Pickup <group_pickup/group_pickup>
   incall
   interconnections/interconnections
   ivr/ivr
   monitoring/monitoring
   moh/moh
   routes
   paging
   parking
   phonebook
   provisioning/provisioning
   sccp/sccp
   schedules/schedules
   sound_files
   Switchboard <switchboard/switchboard>
   users/users
   voicemail
   webrtc/webrtc
   web_services/web_services_access
