.. _sheet_configuration:

*******************
Sheet Configuration
*******************

Sheets can be defined under :menuselection:`Services --> CTI Server --> Models`
in the web interface. Once a sheet is defined, it has to be assigned to an event
in the :menuselection:`Services --> CTI Server --> Events` menu.

Model
   The model contains the content of the displayed sheet.
Event
   Events are actions that trigger the defined sheet. A sheet can be assigned to many events. In
   that case, the sheet will be raised for each event.

.. figure:: images/sheets_configuration.png
   :scale: 85%


General settings
================

.. figure:: images/sheets_configuration_general.png
   :scale: 85%

You must give a name to your sheet to be able to select it later.

The ``Focus`` checkbox is deprecated and will be remove in next version.


.. _custom-call-form:

Sheets
======

Sheets allows to list different fields and associated content to be displayed in XivoCC application such as `CC Agent`.
The information will be automatically laid out in a linear fashion and will be read-only.

List of fields
--------------

Default XiVO sheet example :

.. figure:: images/sheets_configuration_sheet.png
   :scale: 85%

Each field is represented by the following parameters :

* Field title : name of your line used as label on the sheet.
* Field type : define the type of field displayed on the sheet. Supported field types :

  * ``title`` : to create a title on your sheet
  * ``text`` : show a text
  * ``url`` : a simple url link, open your default browser.
  * ``urlx`` : an url button **not implemented**
  * ``phone`` : create a tel: link, you can click to call on your sheet. **not implemented**
  * ``form`` : show the form from an ui predefined. **not implemented**

* **Default value** : if given, this value will be used when all substitutions in the display value field fail.
* **Display value** : you can define text, variables or both. See the :ref:`variable list<sheet-variables>` for more
  information.

.. _sheet-variables:

Variables
---------

Three kinds of variables are available :

  * `xivo-` prefix is reserved and set inside the CTI server:

    * `xivo-where` for sheet events, event triggering the sheet
    * `xivo-origin` place from where the lookup is requested (did, internal, forcelookup)
    * `xivo-direction` incoming or internal
    * `xivo-did` DID number
    * `xivo-calleridnum`
    * `xivo-calleridname`
    * `xivo-calleridrdnis` contains information whether there was a transfer
    * `xivo-calleridton` Type Of Network (national, international)
    * `xivo-calledidnum`
    * `xivo-calledidname`
    * `xivo-ipbxid` (`xivo-astid` in 1.1)
    * `xivo-directory` : for directory requests, it is the directory database the item has been found
    * `xivo-queuename` queue called
    * `xivo-agentnumber` agent number called
    * `xivo-date` formatted date string
    * `xivo-time` formatted time string, when the sheet was triggered
    * `xivo-channel` asterisk channel value (for advanced users)
    * `xivo-uniqueid` asterisk uniqueid value (for advanced users)

  * `db-` prefixed variables are defined when the reverse lookup returns a result.

  For example if you want to access to the reverse lookup full name, you need to define a field
  ``fullname`` in the directory definition, mapping to the full name field in your directory. The
  ``{db-fullname}`` will be replaced by the caller full name. Every field of the directory is
  accessible this way.

  * `dp-` prefixed ones are the variables set through the dialplan (through UserEvent application)

  For example if you want to access from the dialplan to a variable dp-test you
  need to add in your dialplan this line (in a subroutine)::

   UserEvent(dialplan2cti,UNIQUEID: ${UNIQUEID},CHANNEL: ${CHANNEL},VARIABLE: test,VALUE: "Salut")

The ``{dp-test}`` displays Salut.


Event configuration
===================

You can configure a sheet when a specific event is called. For example if you want to receive a
sheet when an agent answers to a call, you can choose a sheet model for the Link event.

The following events are available :

 * Dial: When the member's phone starts ringing for calls on a group or queue
   or when the user receives a call
 * Link: When a user or agent answers a call
 * Unlink: When a user or agent hangup a call received from a queue
 * Incoming DID: Received a call in a DID
 * Hangup: Hangup the call

.. figure:: images/events_configuration.png
   :scale: 85%
