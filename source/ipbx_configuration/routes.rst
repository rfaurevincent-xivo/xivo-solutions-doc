.. _outgoing_calls:

**************
Outgoing Calls
**************

You can configure outgoing calls settings in :menuselection:`Services --> IPBX --> Call Management --> Outgoing calls`.

An outgoing call is composed with a **definition** of a outgoing route (the extension patterns that should match) and a **destination** (a trunk owned by a media server).

.. figure:: images/outgoing_route.png
   :align: center
   :scale: 80%

Definition
----------

A route can define

* **Priority**: A number that will prioritize a route compare to another one, if both routes are available for a same matching extension.
* **Call Pattern**:

  * **Extension**: The pattern that will match an extension. More details on `the Asterisk wiki <https://wiki.asterisk.org/wiki/display/AST/Pattern+Matching>`_.
  * (advanced) **RegExp**: Transformation to apply on the extension once route is found.
  * (advanced) **Target**: Allow to perform filtering like stripping number based on transformation Regexp.
  * (advanced) **Caller ID**: Override the presented number once call is performed.

* **Media server**: define the route only for specific media server.
* **Context**: define the route only for specific context.

Here some examples of how you can take advantage of **RegExp** and **Target**:

========== ======== ======================================================================================================================
 Regexp    Target                                                          Result
========== ======== ======================================================================================================================
(.*)       \\1       get whole called number
(.*)       445      Replace called number with 445 number
14(.*)     \\1       Delete 14 prefix and keep only following part
14(.*)..   \\1       Delete 14 prefix and last two digits and keep the rest (e.g. 1455660 → 556)
(.*)       33\\18    Prefix the called number with 33 and suffix it with 8
14(.*)..   33\\18    Delete 14 prefix and last two digits and then prefix the called number with 33 and suffix it with 8 (1455660 → 335568)
========== ======== ======================================================================================================================

Destination
-----------

Once route is defined and therefore can be selected if an extension match the route, you need to set the wanted destination.
A route destination is materialized by:

* **Trunk**: SIP or Custom trunk that will be used to reach desired network.
* **Subroutine**: Subroutine to apply once route has been selected.


Rights and schedules
--------------------

A route can define **rights** aka call permissions, it means that a call can be discarded if missing the right to use this route.
The same applies for **schedules** where you can define time slots of availability of the route.
