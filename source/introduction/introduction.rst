************
Introduction
************

XiVO solutions is a suite of PBX applications developed by Avencall_, based on several free existing components
including Asterisk_ Play_  Akka_ and Scala_. It provides a solution for enterprises who wish use modern communication services
(IPBX, Unified Messaging, ...) api and application to businesses.

It gives especially access to outsourced statistics, real-time supervision screens,
third-party CTI integration and recording facilities.

.. _Avencall: http://www.avencall.com/
.. _Play: http://www.playframework.com/
.. _Akka: http://akka.io/
.. _Scala: http://www.scala-lang.org/
.. _free software: http://www.gnu.org/philosophy/free-sw.html
.. _Asterisk: http://www.asterisk.org/

XiVO solutions is `free software`_. Most of its distinctive components, and XiVO solutions as a whole, are distributed
under the *GPLv3 license* and or  the *LGPLv3 license*..


XiVO solutions documentation is also available as a downloadable HTML, EPUB or PDF file.
See the `downloads page <https://readthedocs.org/projects/xivo-polaris/downloads/>`_
for a list of available files or use the menu on the lower right.

XiVO History
============

XiVO was created in 2005 by Sylvain Boily (Proformatique SARL).
The XiVO mark is now owned by Avencall_ SAS after a merge between Proformatique SARL and Avencall_ SARL
in 2010.
The XiVO core team now works for Avencall_ in Lyon (France) and Prague (Czech Republic)

* XiVO 1.2 was released  on February 3, 2012.
* XiVO 13.07 was the last version with Asterisk 1.8.21.0
* XiVO 13.08 includes Asterisk 11.3.0 in May 2013
* XiVO 13.25 is running under Wheezy in December 2013
* XiVO 14.02 is now called XiVO Five to celebrate the editor 5th year
* XiVO 15.13 runs Asterisk 13 in July 2015
* XiVO 15.20 is running under Jessie in January 2016
* XiVO 2016.02 starts the new versioning system including XiVO-CC and XiVO-UC modules in October 2016
* XiVO solutions 2016.04 includes new XiVO assistant, web edition, mobile edition and Desktop edition in December 2016
* XiVO solutions 2017.03 in April 2017 is the first Long Term Support version known as Five
* XiVO solutions 2017.11 in October 2017 is the second Long Term Support release known as Polaris

In April 2017 the 3rd, Avencall_ released the fist Long Term Support release of XiVO.solution software suite.
The main goal is to offer a stable version every 6 months even if the team is still doing small and agile iterations of
3 weeks. This release LTS 2017.03 will be the last XiVO Five.

In October 2017, Avencall_ released the second LTS release of XiVO.solutions software suite, Polaris: among other things, it includes major improvements
in the CC Agent application.

Next release is on the way, will be called XiVO Aldebaran and will be available in April 2018.

GPRD
====
XiVO architecture was redesigned to be GPRD compliant by being "privacy by design", which is the GPRD ADN.
We have in this respect refined all the features since Polaris to be 100% compliant.
