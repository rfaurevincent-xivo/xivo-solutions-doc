**************
Contact Center
**************

.. figure:: images/logo_xivo-cc.png
   :scale: 30%

In XiVO, the contact center is implemented to fulfill the following objectives :

* Call routing

  Includes basic call distribution using call queues and skills-based routing

* Agent and Supervisor workstation.

  Provides the ability to execute contact center actions such as: agent login, agent logout and
  to receive real time statistics regarding contact center status

* Statistics reporting

  Provides contact center management reporting on contact center activities

* Advanced functionalities

  Call recording

* Screen Pop-up


.. toctree::
   :maxdepth: 2

   agents/agents
   queues/queues
   ccmanager/ccmanager
   ccagent/ccagent
   qualifications/qualifications
   recording/recording
   callbacks/callbacks
   profile_mgt/profile_mgt
   skillbasedrouting/skillbasedrouting
   XiVOcc Reporting <pack_reporting/index>
   XiVO Reporting <reporting/reporting>
