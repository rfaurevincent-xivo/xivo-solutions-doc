.. _agent:

********************
CC Agent Environment
********************

.. note:: This section describes the CC Agent application features.
   It is available as a web application from your Web Browser.
   It is also available as a *desktop application* with these additionnal features:

     * show integrated popup when receiving call
     * get keyboard shortcut to answer/hangup and make call using :ref:`Select2Call feature <dapp_global_key>`
     * :ref:`handle callto: and tel: links <dapp_call_url>`
     * be able to :ref:`minimize the application to a side bar <dapp_agent_minimize>`

   To install the *desktop application*, see :ref:`the desktop application installation <desktop-application-installation>` page.


**What is the CC Agent application ?**

CC Agent is a Web application for contact center operators. Some parameters for Recording, Callbacks, Queue control, Pause statuses and Sheet popup may be configured.
Instructions can be found in the :ref:`configuration section <agent_configuration>`.

From the interface you will be able to :
 * Manage your activities you are subscribed to receive calls.
 * See the customer history inside your organization when a call is coming
 * Interact with your phone from the call control panel
 * Get some real time statistics of your session

The web application can either be displayed in a minimal bar or be extended as seen in screen shot below when launched as standalone application. See :ref:`desktop application <desktop-application-installation>`.

.. figure:: ccagent.png
    :scale: 80%

.. warning:: The application offers support for the WebRTC lines, currently there's a limitation on complementary services like conference or second call which are partially supported.


Login
=====

.. figure:: ccagent-login.png
    :scale: 50%

Enter your XiVO client username, password and phone set you want to logged to on the login page.

If you are using Kerberos authentication and enabled SSO (see :ref:`kerberos-configuration`), then you only have to set your phone set number, the authentication and login will be done automatically:

.. _ccagent_statistics:

Statistics
==========

At the top of the application, :ref:`computed statistics <agent_statistics>` from XUC are displayed to monitor the agent activity. Simply hover the icon to know its definition.

They are updated once current action is over.

.. figure:: ccagent-stats.png
    :scale: 80%

Those statistics are clickable and display dynamic charts on top of the counters. The goal is to offer a global view to agents on their activity during the day. 
 
There is two different charts displayed. 

The first one is a bar chart focusing on the time unit : 

- Total Available Time 
- Total Pause Time 
- Inbound ACD Calls Total Time
- Outbound Calls Total Time
- Total Wrapup Time

On click on each one of those buttons, the bar chart will be displayed.
You can hover each bar to have the detailed time spent for a specific indicator (HH:MM:SS). 

.. figure:: ccagent-barChart.png
    :scale: 80%

The second one is a donut chart focusing on the number of calls : 

- Number of Inbound ACD calls
- Number of Inbound Answered ACD Calls
- Number of Outbound Calls

On click on each one of those buttons, the donut chart will be displayed.
You can hover each part of the donut to have the number of calls for one indicator.  

.. figure:: ccagent-donut.png
    :scale: 80%

The charts are updated once current action is over.


Activities
==========

Once logged in you are automatically redirected to ``activities`` view, this view contains the list of activities you are registered in.

.. figure:: ccagent-activities.png
    :scale: 80%

Hovering an activity triggers a popover which displays some real-time statistics about call distribution in this queue.
You may also call or transfer a call to an activity using the displayed phone icon

It is possible to filter on `favorite` activities just by clicking ``My activities`` check box.


Activity Management
-------------------

You can manage ``subscription`` if allowed globally for the application (see :ref:`agent_configuration`). If enabled you will be able to enter/quit an activity just by clicking on checkbox associated to it.

Each time you enter an activity, it is automatically added to your favorites. At any time you can remove it by clicking on `minus` sign next to the name in ``My activities`` view.

.. note:: You can remove an activity if and only if you are not already registered in.

It's also possible to enter all your favorites activities just by one click on ``All`` checkbox.

.. figure:: ccagent-favorites.png
    :scale: 80%


.. _agent_activity_colors:

Activity Colors
---------------

Activity color changes depending on call waiting and agent status:

  * **Grey**: No call, No agents logged in this activity
  * **Green**: At least one agent logged and available in this activity
  * **Orange**: At least one agent logged, but no agents available in this activity
  * **Red**: No agents logged in this activity, but one or more waiting calls in this activity

.. _agent_activity_waitingcalls:

Activity Waiting Calls
----------------------

A little badge displays the number of waiting calls in each activity. The sum of all waiting calls in the agent activities is displayed in top menu and refreshed in real time.

.. figure:: ccagent-waiting-call.png
    :scale: 80%

User directory search
=====================

When using the Agent interface, you can at any time search for a user existing in your directory:

.. figure:: ccagent-search.png
    :scale: 90%

Known limitations
-----------------

- The search support is limited so far to simple words without spaces and simple characters.


.. _agent_list:

Agent list
==========

When clicking on the ``Agents`` menu, you will see all the agents of **your group**. By hovering one of them, you will quickly find his current state (ready, calling, in pause...)

.. figure:: ccagent-list.png
    :scale: 90%

A simple click on the phone icon when agent is hovered will trigger a call to its phone number associated.

By default, agents are shown only if they are logged in (checkbox *Logged* checked). By unchecking the checkbox *Logged*, you will see all the agents of your group even if they are logged out.


Call tracking & control
=======================

When using the Agent interface, you will see your current calls at the top of the screen:

.. figure:: ccagent-call-info.png
    :scale: 90%

This panel will display the current caller name & number and also the associated activity if the call came from one.
You also have two indicator on the right side letting you know if the call is currently recorded and if the call is currently listened by a supervisor.

By hovering your mouse on the call line, an action pane will slide to display action button on the related call. The available buttons depend on the call state.

.. figure:: ccagent-call-hover.png
    :scale: 90%

.. figure:: ccagent-call-hold-hover.png
    :scale: 90%

The Agent interface use Desktop notification for incoming calls and notify long calls on hold, but this feature needs to be enabled from the browser window when logging in:

  .. figure:: agent-notification.png
      :scale: 90%

.. _uc_hold_notification:

On hold notifications
---------------------

You can be notified if you forget a call in hold for a long time, see :ref:`configuration section <hold_notification>`.

.. figure:: agent-hold-notification.png
    :scale: 90%

Known limitations
-----------------

- The transfer support is limited, to transfer a call you need to write the number to the search field, then click
  the attended transfer button and then you can complete the transfer by the complete transfer button.

- Conference and second call except the attended transfer are not supported for agents with webRTC lines.

Also see the phone integration :ref:`Known limitations <phone_integration_limitations>`.


.. _agent_agent_history:

Agent Call history
==================

First menu ``History`` tab is displaying the call history of connected agent.

Information shown in the call list are :

- *destination number* or *name* of callee if call is *emitted*

- *source number* or *name* of caller if a call is *received* or *missed*

- Call icon status and call start date

By clicking on phone icon you will be able to call back if needed.

.. warning:: Pay attention that agent history is **not** the phone device history, but his call activity independently the device he is connected to. Actually when agent is logged out, if a call is received on his last used phone, nothing will be shown in his history.

.. note:: A call answered by another agent from the queue, will appear as answered in the history of the first agent.

.. figure:: ccagent-history.png
    :scale: 80%


.. _agent_customer_history:

Customer Call History
=====================

Meanwhile phone is ringing or discussion is ongoing, it is possible to have a quick overview of the customer call history of the caller just by clicking ``information`` menu.

The customer history is displayed from most recent to last one with an icon to know quickly waiting time of current or previous call :

- agent with grey `play` icon states for current call
- agent with red `bubble` icon states for an unanswered call
- agent with green `bubble` icon states for an answered call

.. note:: If a call is answered (`green icon`), hovering the line will give the name of the agent who took the call.

.. figure:: ccagent-pathway.png
    :scale: 80%

Customer Call Context
=====================

Second tab of ``information`` menu displays all attached data enriched to the ongoing call or display Sheet fields if you are using :ref:`sheet_configuration`.

The customer history is displayed from most recent to last one with an icon to know quickly waiting time of current or previous call :

.. figure:: ccagent-context.png
    :scale: 80%

It's also possible to trigger either to open a web page, see :ref:`screen_popup` or completely integrate a third party application while agent is having calls, see :ref:`xucmgt_3rd_configuration`.

Callbacks
=========

This view allows to manage callback request see :ref:`Processing Callbacks with CCAgent <callbacks_with_ccagent>` for details.

.. _agent_webrtc_ringing_device:

WebRTC - Ringing device selection
=================================

When receiving a call, your computer will play a ringing sound however you can choose to play this sound on a separate audio device than the default selected by your operating system. For example, on a configuration with a headset, you may choose to have this device as your default device but you can override this selection to play the ringing sound on your computer instead.
You can choose the output device for the ringing sound by clicking on the top-left audio settings menu and then select the prefered device:

.. figure:: ccagent-audio-settings.png
   :scale: 100%

.. figure:: ringing-device-ccagent.png
   :scale: 100%

This feature is only available when using a WebRTC line.
