.. highlightlang:: rest

.. _ccmanager:

*************************
Contact Center Management
*************************

Introduction
============


.. figure:: ccmanager.png
    :scale: 50%

CCmanager is a web application to manage and supervise a contact center, different menus are available from hamburger icon with following features:

1. **Global view**: Queues and penalties with real time activity of each agent, possible options are

	a. Enable Compact view (remove queue statistics)
	b. Show/Hide agents that are not logged in
2. **Group view**: Distribution of agents per queues
3. **Queue view**: Activity per queue
4. **Agent view**: Activity per agent, possible actions are [#f1]_

	a. Login / Logout
	b. Pause / Available
	c. Listen [#f2]_
	d. Call [#f2]_
5. **Callback view**: List of callbacks
6. **Qualification view**: To export calls qualification

Start the application : http://<xucmgt:port>/ccmanager


Authorizations and Access Control
=================================

Access to the application is restricted to authorized users (see :ref:`ccmanager-security`).


Queue statistics
================

.. figure:: queue-stats.png
    :scale: 100%

This diagram shows some aggregation about statistics collected from queue activity

1. Percentage of calls answered before 15s
2. Percentage of calls abandoned after 15s
3. Number of available agents to take calls
4. Number of pending calls not answered yet


Editing Agent Configuration
===========================

.. figure:: agent-edit.png
    :scale: 50%

This interface allows a user to change queue assignement and the associated penalty. The queue table display the following columns

* "Number": The queue number
* "Name": The queue name
* "Penalty": The active penalty for the corresponding queue
* "default": The default penalty for the corresponding queue

The queue/active penalty couples can be saved as default configuration by clicking the "Set default" button, then "Save".
The queue/default penalty couples can be saved as active configuration by clicking the "Set current" button, then "Save".

.. note:: Removing an agent from a queue

    * Emptying the penalty textbox and saving will remove the queue from the active configuration for the agent.
    * Emptying the default textbox and saving will remove the queue from the default configuration for the agent.


Multiple Agent Selection
========================

From agent view you are able to add or remove more than one agent at the same time.

.. figure:: multipleagentselect.png
    :scale: 80%

Once the agent selection is done, click on the edit button to display the configuration window

.. figure:: agentsconfiguration.png
    :scale: 80%

Click on the plus button to add a queue for selection, click on the minus button to remove a queue to the selection.
Once queue to add or removed are choosen, click on save button to apply your configuration change.

Click on "Apply default configuration" to apply existing default configuration to all selected users and make it the active configuration. This action only affects users with an existing default configuration, agents whithout default configuration remain unchanged.


Agent Base Configuration
========================

From the agent view, after selecting one or more agents, you can create a base configuration by clicking on one of the menu item in the following drop down:

.. figure:: ccmanager-base-batch-dropdown.png
   :scale: 80%

* 'Create base configuration' will allow you to create a base configuration from scratch for all the selected agents.
* 'Create base configuration from active configuration' will allow you to create a base configuration using the selected agents active configuration. The queue membership and penalty populated will be built based on the merged membership of all the selected agents. In case of conflict, the lowest penalty will be used.

In both cases, you will be able to review your changes before applying them. The 'Create base configuration' popup is similar to the single agent edition popup:

.. figure:: ccmanager-base-batch-create.png
   :scale: 80%

The queue table display the following columns:

* "Number": The queue number
* "Name": The queue name
* "Penalty": The active penalty for the corresponding queue

Click on the plus button to add a queue for selection. Once your configuration is complete, click on save button to apply your configuration change.

.. _ccmanager_base_configuration_filter:

Applying Default Configuration
------------------------------

In order to re-apply or apply a default configuration, you may select agent whose base configuration is different from active configuration.

In the agent view, you will find a new column (Base config.) displaying if the base configuration is different from the active one:

.. figure:: ccmanager-base-filter.png
   :scale: 80%

The possible values for this field are:

* "n/a": The base configuration is not available for this agent
* "Ok" : The base configuration match the active configuration
* "Different": The base configuration **does not** match the active configuration

You can use this column to filter agent whose base configuration is different from the active one and then apply the default configuration by using the "Edit agent" option.

.. _ccmanager_queue_recording_activation:

Queue Recording
===============

Description
-----------

Once you setup queue recording in XiVO (see :ref:`queue_recording_configuration`), visual indicators are displayed next to queue name in **Global view**. Respectively following icons represents **recording mode** set on the queue.

.. figure:: ccmanager_queue_record_info.png

Furthermore shortcut action is displayed in the left menu to control the activation / deactivation of **all** recorded queues.
The switch will change its position in case the queue's recording status is activated / deactivated through XiVO Web Interface accordingly.

Global recording activation
---------------------------

The switch button will either activate all queues configured with **recording mode** set (*Recorded* or *Recorded on demand*), or stop the recording feature for all queues.

.. figure:: ccmanager_queue_recording.png
   :scale: 80%


.. note:: Action is applied only for next calls. Ongoing call recording is not started nor stopped when switch is triggered.


Thresholds
==========

Color thresholds can be defines for the waiting calls counter and the maximum waiting time counter

.. figure:: ccmanager_thresholds.png
    :scale: 80%

Applies to the queue view and the global view

.. _ccmanager_callbacks:

Callbacks
=========

This view allows to manage callback request see :ref:`Managing Callbacks Using CCManager <callbacks_with_ccmanager>` for details.

.. _ccmanager_qualifications:

Exporting qualifications
========================

This view allows to export calls qualification see :ref:`Call Qualifications <call_qualification>`.

To export the qualification answers, open **Qualifification View** page.

.. figure:: ccmanager_qualifications_export.png
   :scale: 85%

Select the date from, date to and queue. Then click to Download button.
This will open new page with CSV file to download.

   .. warning::

      When exporting data from the same day, select **date to** to be +1 day of **date from**.



.. rubric:: Footnotes
.. [#f1] Available actions depend on the state of the agent
.. [#f2] Only supervisors which have their own line can listen to or call agents, not supported for mobile supervisors, a line has to be affected to supervisors in xivo
