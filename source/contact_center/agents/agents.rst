******
Agents
******

Introduction
============

   *A call center agent is the person who handles incoming or outgoing customer 
   calls for a business. A call center agent might handle account inquiries, 
   customer complaints or support issues. Other names for a call center agent 
   include customer service representative (CSR), telephone sales or service 
   representative (TSR), attendant, associate, operator, account executive 
   or team member.*

   -- SearchCRM

In this respect, agents in XiVO have no fixed line and can login from any registered device.


Getting Started
===============

* Create a user with a SIP line and a provisioned device.
* Create agents.
* Create a queue adding created agent as member of queue.


Creating agents
================

Service > Call center > Agents > General
----------------------------------------

These settings are specific for a given agent.


Service > Call center > Agents > Users
--------------------------------------

These settings are specific for a given agent.


Service > Call center > Agents > Queues
---------------------------------------

These settings are specific for a given agent.


Service > Call center > Agents > Advanced
-----------------------------------------

These settings are specific for a given agent.


Service > IPBX > General settings > Advanced > Agent
----------------------------------------------------

These settings are global for all agents.

Agent with external line
========================

XiVO system agents can be external to the system, the agent can use it's personal PSTN line, mobile phone or a terminal
connected to some other PBX system. We call these remote lines external line. The same agent can login to a standard
line, or to an external line. The choice is done via the line number on the login page.

Creating agents with external line
----------------------------------

Agent settings are the same, the only difference is in the line which is used by the agent. You must create a user with
a custom line:

* Start by creating a standard user, when creating a line pick up a line number and choose Customized line protocol.

.. figure:: user_with_custom_line_creation.png
    :scale: 90%

* Then save the user, go to lines listing and edit the created custom line.

.. figure:: custom_line_editing.png

* Replace the line Interface by a string with following format:

  Local/EXTERNAL_LINE_NUMBER@default/n

where EXTERNAL_LINE_NUMBER@default is the number and context which can be used to join the remote phone, for a french
mobile phone it would usually be `0xxxxxxxxx@default`.

Usage
-----

Tha agent has to login using the custom line, the standard ccagent features are available. However, due to external line
some phone control features are not available - the agent can't answer, put on hold or transfer calls from the ccagent
interface. On the XiVO side, please ensure that the call distributed to the agent is canceled if the agent doesn't answer
before the call is answered by for example the voicemail.
