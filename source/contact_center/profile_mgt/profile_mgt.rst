.. _profile_mgt:

******************
Profile Management
******************

With what is called the Configuration Management Server one can specify the profile of a user.

To do that one has to:

* log in the Configuration management server accessible at ``https://XIVO_PBX/configmgt``,
* as *Superadmin* (whose login is ``avencall``)

When logged in as *Superadmin* one can:

* attribute a profile (see :ref:`profile_mgt-profile`) to a user (note that the user must have a CTI login to be listed),
* create callback lists (see :ref:`callback_lists`)
* and create callback periods (see :ref:`callback_periods`)


.. _profile_mgt-profile:

Profiles Definition
===================

Profiles and their rights are summed up in the following table:

+----------------------+--------------------------------------------------------------------------------+
| Profile              | Application                                                                    |
|                      +-------------------------------------+------------------------------------------+
|                      | **CC Manager**                      | **Recording**                            |
|                      +----------+--------------------------+----------+-------------------------------+
|                      | *Access* | *Actions*                | *Access* | *Actions*                     |
+======================+==========+==========================+==========+===============================+
| **Administrator**    | Yes      | All                      | Yes      |  * all recording              |
|                      |          |                          |          |  * recordings filtering       |
+----------------------+----------+--------------------------+----------+-------------------------------+
| **Supervisor** [4]_  | Yes      | * All on its queues [1]_ | Yes      | Its queues' [1]_ recordings   |
|  * w/ *Recording*    |          | * Recording switch       |          |                               |
+----------------------+          +--------------------------+----------+-------------------------------+
| **Supervisor** [4]_  |          | * All on its queues [1]_ | No       | N.A. [2]_                     |
|  * wo/ *Recordings*  |          | * No recording switch    |          |                               |
+----------------------+----------+--------------------------+----------+-------------------------------+
| **Teacher**          | No       | N.A. [2]_                | Yes      | * Its queues' [1]_ recordings |
|                      |          |                          |          | * During configured period    |
+----------------------+----------+--------------------------+----------+-------------------------------+
| No profile           | No [3]_  | N.A. [2]_                | No       | N.A.                          |
+----------------------+----------+--------------------------+----------+-------------------------------+

.. [1] i.e. attributed queues to the user via the Config Mgt. Note also that agents groups should be attributed accordingly.
.. [2] Not Applicable
.. [3] Depending on the :ref:`ccmanager-security` configuration
.. [4] See :ref:`profile_mgt-supervisor` section


.. _profile_mgt-supervisor:

Supervisor Profile Specificities
--------------------------------

Compared to other profiles, supervisor profile has specific rights. That is, when logged in the Configuration Management Server, a *Supervisor* can:

* attribute a profile **Teacher** to another user (to a subset of its own attributed queues),
* and create callbacks list (on its own attributed queues)


Impact on CC Agent
==================

Any agent can log in the CC Agent application.

Then, when logged in the CC Agent, this agent can see all the queues in the Activities view.

.. important:: **But** if an agent *is also a Supervisor*. When this agent is logged in the CC Agent,
   it will see **only** the set of queues and agents as it was given him in the *Supervisor* profile via the Configuration Management Server.


